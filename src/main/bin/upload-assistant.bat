:: upload-assistant
:: Copyright (c) 2015 Washington University
:: Author: Rick Herrick <jrherrick@wustl.edu>
@ECHO OFF
set LIB=%~dp0..\lib
"%JAVA_HOME%\bin\java" -cp "%LIB%\${project.artifactId}-${project.version}-jar-with-dependencies.jar" ${mainClass} %*
