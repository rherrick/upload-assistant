/*
 * upload-assistant: org.nrg.xnat.upload.ui.ChooseLocaleAction
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.ui;

import org.nrg.xnat.upload.application.UploadAssistantSettings;
import org.nrg.xnat.upload.util.LocaleWrapper;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Locale;

public class ChooseLocaleAction extends AbstractAction {
    public ChooseLocaleAction(final Component component, final UploadAssistantSettings settings) {
        super(Messages.getMessage("menu.locale.title"));
        putValue(Action.MNEMONIC_KEY, KeyEvent.VK_L);
        _component = component;
        _settings = settings;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        final String message = Messages.getMessage("locale.message");
        final JPanel panel = new JPanel();
        final List<LocaleWrapper> locales = Messages.getDisplayLocales();
        final JComboBox<LocaleWrapper> chooser = new JComboBox<>(locales.toArray(new LocaleWrapper[locales.size()]));
        final JLabel label = new JLabel(message);

        panel.add(label);
        panel.add(chooser);

        chooser.setSelectedItem(Messages.getLocaleWrapper());

        _log.info("Now performing the action {}", event.getActionCommand());
        final int confirm = JOptionPane.showOptionDialog(_component, panel, (String) getValue(Action.NAME), JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
        if (confirm == JOptionPane.YES_OPTION) {
            final Object item = chooser.getSelectedItem();
            _settings.setLocale(item != null ? ((LocaleWrapper) item).toLocale() : Locale.getDefault());
        }
    }

    private static final Logger _log = LoggerFactory.getLogger(ChooseLocaleAction.class);

    private final Component               _component;
    private final UploadAssistantSettings _settings;
}
