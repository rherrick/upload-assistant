/*
 * upload-assistant: org.nrg.xnat.upload.ui.ExitAction
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.ui;

import org.nrg.xnat.upload.util.Messages;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import javax.swing.*;

/**
 * This class will create and dispatch a WINDOW_CLOSING event to the active
 * window.  As a result any WindowListener that handles the windowClosing
 * event will be executed. Since clicking on the "Close" button of the
 * frame/dialog or selecting the "Close" option from the system menu also
 * invoke the WindowListener, this will provide a commen exit point for
 * the application.
 */
public class ExitAction extends AbstractAction {
    public ExitAction() {
        super(Messages.getMessage("menu.exit.title"));

        putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        //  Find the active window before creating and dispatching the event
        final Window window = KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow();

        if (window != null) {
            window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
        }
    }
}
