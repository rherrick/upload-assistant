/*
 * upload-assistant: org.nrg.xnat.upload.ui.Constants
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.ui;

import netscape.javascript.JSObject;

import java.awt.*;
import java.util.Date;

public class Constants {
    public static final Date   NO_DATE     = new Date(0);
    public static final int    SPACING     = 4;
    public static final Cursor LINK_CURSOR = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);

    public static final String APPLICATION_NAME         = "application.name";
    public static final String APPLICATION_VERSION      = "application.version";
    public static final String APPLICATION_VENDOR       = "application.vendor";
    public static final String APPLICATION_BUILD_DATE   = "application.build.date";
    public static final String APPLICATION_BUILD_NUMBER = "application.build.number";
    public static final String APPLICATION_BUILD_COMMIT = "application.build.commit";

    public static JSObject getContext() {
        return _context;
    }

    public static void setJSContext(final JSObject context) {
        _context = context;
    }

    public static String getWindowName() {
        return _windowName;
    }

    public static void setWindowName(final String windowName) {
        _windowName = windowName;
    }

    private static JSObject _context    = null;
    private static String   _windowName = "_self";
}
