/*
 * upload-assistant: org.nrg.xnat.upload.ui.XnatServerAttributesPanel
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *  
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.ui.TraceableGridBagPanel;
import org.nrg.xnat.upload.application.UploadAssistantSettings;
import org.nrg.xnat.upload.net.XnatServer;
import org.nrg.xnat.upload.util.Messages;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public final class XnatServerConfigurationPanel extends TraceableGridBagPanel implements ActionListener {
    public XnatServerConfigurationPanel(final XnatServerAuthenticationPage parent, final UploadAssistantSettings settings, final boolean trace) {
        super(trace);

        _parent = parent;
        _settings = settings;

        _address = new JTextField() {{
            setToolTipText(Messages.getMessage("xnatserverconfigurationpanel.controls.address.tooltip"));
            addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(final FocusEvent event) {
                    super.focusLost(event);
                    if (!configureAllowUnverifiedHttps(getText())) {
                        _username.requestFocusInWindow();
                    }
                }
            });
        }};
        _allowUnverifiedHttps = new JCheckBox(Messages.getMessage("xnatserverconfigurationpanel.controls.unverifiedssl")) {{
            setToolTipText(Messages.getMessage("xnatserverconfigurationpanel.controls.unverifiedssl.tooltip"));
        }};
        _username = new JTextField();

        _save = new JButton(Messages.getMessage("button.save")) {{
            addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent event) {
                    final XnatServer server = getXnatServerFromControls();
                    if (server == null) {
                        final String address = _address.getText();
                        UIUtils.handleUserError(parent, Messages.getDialogFormattedMessage("xnatserverconfigurationpanel.invalidserver.message", address), Messages.getMessage("xnatserverconfigurationpanel.invalidserver.title", address), JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        _settings.saveServer(server);
                        _settings.setCurrentXnatServer(server);
                        _address.setText("");
                        _allowUnverifiedHttps.setSelected(false);
                        _username.setText("");
                    }
                    _parent.showAuthenticationPanel();
                }
            });
        }};

        _cancel = new JButton(Messages.getMessage("button.cancel")) {{
            addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent event) {
                    _parent.showAuthenticationPanel();
                }
            });
        }};

        add(new JLabel(Messages.getMessage("xnatserverconfigurationpanel.controls.address")) {{
            setLabelFor(_address);
        }}, new GridBagConstraints() {{
            gridx = 0;
            gridy = 0;
            gridwidth = 1;
            weightx = 0;
            anchor = GridBagConstraints.WEST;
            fill = GridBagConstraints.HORIZONTAL;
            insets = new Insets(5, 5, 5, 5);
        }});
        add(_address, new GridBagConstraints() {{
            gridx = 1;
            gridy = 0;
            gridwidth = 3;
            weightx = 1;
            anchor = GridBagConstraints.WEST;
            fill = GridBagConstraints.HORIZONTAL;
            insets = new Insets(5, 5, 5, 5);
        }});
        add(_allowUnverifiedHttps, new GridBagConstraints() {{
            gridx = 1;
            gridy = 1;
            gridwidth = 3;
            weightx = 1;
            anchor = GridBagConstraints.WEST;
            fill = GridBagConstraints.HORIZONTAL;
            insets = new Insets(5, 5, 5, 5);
        }});
        add(new JLabel(Messages.getMessage("xnatserverconfigurationpanel.controls.username")) {{
            setLabelFor(_username);
        }}, new GridBagConstraints() {{
            gridx = 0;
            gridy = 2;
            gridwidth = 1;
            weightx = 0;
            anchor = GridBagConstraints.WEST;
            fill = GridBagConstraints.HORIZONTAL;
            insets = new Insets(5, 5, 5, 5);
        }});
        add(_username, new GridBagConstraints() {{
            gridx = 1;
            gridy = 2;
            gridwidth = 3;
            weightx = 1;
            anchor = GridBagConstraints.WEST;
            fill = GridBagConstraints.HORIZONTAL;
            insets = new Insets(5, 5, 5, 5);
        }});
        add(new JPanel(new FlowLayout()) {{
            add(_save);
            add(_cancel);
        }}, new GridBagConstraints() {{
            gridx = 3;
            gridy = 3;
            gridwidth = 1;
            weightx = 0;
            anchor = GridBagConstraints.WEST;
            fill = GridBagConstraints.HORIZONTAL;
            insets = new Insets(5, 5, 5, 5);
        }});

        setVisible(true);
        adjustPanelState();
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        adjustPanelState();
    }

    public void setXnatServer(final XnatServer server) {
        final String address = server.getAddress();
        _address.setText(address);
        configureAllowUnverifiedHttps(address, BooleanUtils.isTrue(server.isAllowUnverifiedHttps()));
        _username.setText(server.getUsername());
        adjustPanelState();
    }

    public String validateContents() {
        if (StringUtils.isNoneBlank(_address.getText(), _username.getText())) {
            _save.setEnabled(true);
            return _settings.getXnatServerNames().size() > 0 ? Messages.getMessage("xnatserverconfigurationpanel.saveorcancel") : Messages.getMessage("xnatserverconfigurationpanel.save");
        }
        _save.setEnabled(false);
        return Messages.getMessage("xnatserverconfigurationpanel.completeconfig");
    }

    /**
     * Gets a transient instance of {@link XnatServer} based on the values currently specified in the page controls. If
     * invalid values are specified (e.g. malformed URL), this method returns null.
     *
     * @return A transient instance of {@link XnatServer} if valid values are specified, null otherwise.
     */
    protected XnatServer getXnatServerFromControls() {
        final String address = _address.getText();
        final Boolean allowUnverifiedHttps = _allowUnverifiedHttps.isEnabled() && _allowUnverifiedHttps.isSelected();
        final String username = _username.getText();

        final XnatServer configuredXnatServer = new XnatServer(address, allowUnverifiedHttps, username);
        if (_settings.hasServer(configuredXnatServer.getName())) {
            _settings.saveServer(configuredXnatServer);
        }
        return configuredXnatServer;
    }

    protected void adjustPanelState() {
        final String address = _address.getText();
        configureAllowUnverifiedHttps(address);
        _save.setEnabled(StringUtils.isNotBlank(address) && StringUtils.isNotBlank(_username.getText()));
        _cancel.setEnabled(_settings.getXnatServerNames().size() > 0);
    }

    private boolean configureAllowUnverifiedHttps(final String address) {
        final boolean isHttp = StringUtils.startsWith(address, "http://");
        if (isHttp) {
            _allowUnverifiedHttps.setSelected(false);
            _allowUnverifiedHttps.setEnabled(false);
        }
        return !isHttp;
    }

    private void configureAllowUnverifiedHttps(final String address, final boolean allowUnverifiedHttps) {
        final boolean isHttp = configureAllowUnverifiedHttps(address);
        if (!isHttp) {
            _allowUnverifiedHttps.setSelected(allowUnverifiedHttps);
        }
    }

    private final XnatServerAuthenticationPage _parent;
    private final UploadAssistantSettings      _settings;
    private final JTextField                   _address;
    private final JCheckBox                    _allowUnverifiedHttps;
    private final JTextField                   _username;

    private final JButton _save;
    private final JButton _cancel;
}
