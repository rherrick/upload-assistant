/*
 * upload-assistant: org.nrg.xnat.upload.ui.UploadResultPanel
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import org.apache.commons.lang3.StringUtils;
import org.nrg.xnat.upload.util.Messages;

import javax.swing.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public final class UploadResultPanel extends JPanel {
    public UploadResultPanel(final String label, final URL url) {
        final boolean isArchived = getIsArchived(url);
        final JLabel link = getLinkForResource(url);

        final Box box = new Box(BoxLayout.Y_AXIS);
        add(box);

        final String destination = Messages.getMessage(isArchived ? Messages.VOCABULARY_ARCHIVE : Messages.VOCABULARY_PREARCHIVE);
        final String message = StringUtils.isBlank(label)
                               ? Messages.getPageFormattedMessage(Messages.UPLOADRESULTPANEL_SUCCESS_NOLABEL, destination)
                               : Messages.getPageFormattedMessage(Messages.UPLOADRESULTPANEL_SUCCESS_WITHLABEL, label, destination);
        box.add(new JLabel(message));
        box.add(Box.createVerticalStrut(Constants.SPACING));
        box.add(link);
    }

    private boolean getIsArchived(final URL url) {
        final List<String> atoms = Arrays.asList(StringUtils.split(url.getPath(), "/"));
        return StringUtils.equalsIgnoreCase("archive", atoms.get(atoms.indexOf("data") + 1));
    }

    public JLabel getLinkForResource(final URL url) {
        final String message = Messages.getPageFormattedMessage("body.passthrough", Messages.getFormattedLink(url, Messages.UPLOADRESULTPANEL_DEST_LINK));
        final JLabel link = new JLabel(message);
        link.setCursor(Constants.LINK_CURSOR);
        try {
            link.addMouseListener(new UriClickListener(url.toURI()));
        } catch (URISyntaxException e) {
            UIUtils.handleApplicationError(this, Messages.getMessage("uploadresultpanel.error.message", e.getMessage()), Messages.getMessage("uploadresultpanel.error.title"), JOptionPane.ERROR_MESSAGE);
        }
        return link;
    }
}
