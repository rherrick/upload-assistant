/*
 * upload-assistant: org.nrg.xnat.upload.ui.ExitAction
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.ui;

import org.apache.commons.lang3.StringUtils;
import org.nrg.xnat.upload.application.UploadAssistantSettings;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AboutAction extends AbstractAction {

    public AboutAction(final JFrame component, final UploadAssistantSettings settings) {
        super(Messages.getMessage("menu.about.title", settings.getProperty(Constants.APPLICATION_NAME)));
        _component = component;
        _name = settings.getProperty(Constants.APPLICATION_NAME);
        _version = settings.getProperty(Constants.APPLICATION_VERSION);
        _vendor = settings.getProperty(Constants.APPLICATION_VENDOR);
        _buildDate = settings.getProperty(Constants.APPLICATION_BUILD_DATE);
        _buildNumber = settings.getProperty(Constants.APPLICATION_BUILD_NUMBER);
        _buildCommit = settings.getProperty(Constants.APPLICATION_BUILD_COMMIT, Messages.getMessage("about.message.commit.unknown"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        _log.info("Now performing the action {}", e.getActionCommand());
        AboutDialog.showDialog(_component, _name, _version, _vendor, _buildNumber, _buildCommit, _buildDate);
    }

    private static class AboutDialog extends AssistantDialog {
        public static void showDialog(final Frame parent, final String name, final String version, final String vendor, final String buildNumber, final String buildCommit, final String buildDate) {
            final AboutDialog dialog = new AboutDialog(parent, name, version, vendor, buildNumber, buildCommit, parseBuildDate(buildDate));
            dialog.setModal(true);
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setVisible(true);
        }

        private static Date parseBuildDate(final String buildDate) {
            try {
                return FORMATTER.parse(StringUtils.removePattern(buildDate, "Z$"));
            } catch (ParseException e) {
                return new Date();
            }
        }

        private AboutDialog(final Frame parent, final String name, final String version, final String vendor, final String buildNumber, final String buildCommit, final Date buildDate) {
            super(parent, Messages.getMessage("menu.about.title", name), true);

            final ImageIcon backgroundImage = new ImageIcon(getClass().getResource("/images/xnat-splash.png"));
            setSize(backgroundImage.getIconWidth(), backgroundImage.getIconHeight());

            final JPanel panel = new JPanel(new BorderLayout()) {
                @Override
                protected void paintComponent(Graphics g) {
                    super.paintComponent(g);
                    g.drawImage(backgroundImage.getImage(), 0, 0, getWidth(), getHeight(), this);
                }

                @Override
                public Dimension getPreferredSize() {
                    Dimension size = super.getPreferredSize();
                    size.width = Math.max(backgroundImage.getIconWidth(), size.width);
                    size.height = Math.max(backgroundImage.getIconHeight(), size.height);

                    return size;
                }
            };

            final JLabel nameAndVersionLabel = new JLabel(Messages.getMessage("about.message.name", name, version));
            nameAndVersionLabel.setOpaque(false);
            final JLabel vendorLabel = new JLabel(Messages.getMessage("about.message.vendor", vendor));
            vendorLabel.setOpaque(false);
            final JLabel buildLabel = new JLabel(Messages.getMessage("about.message.build", buildNumber, buildCommit, buildDate));
            buildLabel.setOpaque(false);

            final JPanel text = new JPanel(new GridLayout(0, 1, 5, 5));
            text.setBorder(new EmptyBorder(10, 10, 10, 10));
            text.setOpaque(false);
            text.add(nameAndVersionLabel);
            text.add(vendorLabel);
            text.add(buildLabel);

            final JButton close = new JButton(Messages.getMessage("button.close")) {{
                addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        dispose();
                    }
                });
            }};

            panel.add(text, BorderLayout.NORTH);
            panel.add(close, BorderLayout.SOUTH);

            add(panel);
        }

        private static final DateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    }

    private static final Logger _log = LoggerFactory.getLogger(AboutAction.class);

    private final JFrame _component;
    private final String _name;
    private final String _version;
    private final String _vendor;
    private final String _buildDate;
    private final String _buildNumber;
    private final String _buildCommit;
}
