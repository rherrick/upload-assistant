/*
 * upload-assistant: org.nrg.xnat.upload.ui.UriClickListener
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.ui;

import org.nrg.xnat.upload.util.Messages;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URI;

public class UriClickListener implements MouseListener {

    private final URI _uri;

    public UriClickListener(final URI uri) {
        _uri = uri;
    }

    @Override
    public void mouseClicked(final MouseEvent event) {
        try {
            final Desktop desktop = Desktop.getDesktop();
            if (desktop.isSupported(Desktop.Action.BROWSE)) {
                desktop.browse(_uri);
            } else {
                UIUtils.handleUrlClick(event.getComponent(), _uri.toString());
            }
        } catch (IOException e) {
            UIUtils.handleApplicationError(event.getComponent(),
                                           Messages.getDialogFormattedMessage("uriclicklistener.error.message", e.getMessage()),
                                           Messages.getMessage("uriclicklistener.error.title"),
                                           JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void mousePressed(final MouseEvent event) {

    }

    @Override
    public void mouseReleased(final MouseEvent event) {

    }

    @Override
    public void mouseEntered(final MouseEvent event) {

    }

    @Override
    public void mouseExited(final MouseEvent event) {

    }
}
