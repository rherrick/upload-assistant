/*
 * upload-assistant: org.nrg.xnat.upload.ui.VariableAssignmentManager
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import com.google.common.collect.Lists;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.xnat.upload.data.SessionVariable;
import org.nrg.xnat.upload.data.SessionVariable.InvalidValueException;
import org.nrg.xnat.upload.data.SessionVariableConsumer;
import org.nrg.xnat.upload.data.ValueListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class VariableAssignmentManager {
    public final static GridBagConstraints labelConstraint   = new GridBagConstraints();
    public final static GridBagConstraints valueConstraint   = new GridBagConstraints();
    public final static GridBagConstraints messageConstraint = new GridBagConstraints();

    static {
        labelConstraint.gridx = 0;
        labelConstraint.weightx = 0.1;

        valueConstraint.gridx = 1;
        valueConstraint.fill = GridBagConstraints.HORIZONTAL;
        valueConstraint.weightx = 0.6;

        messageConstraint.gridx = 2;
        messageConstraint.fill = GridBagConstraints.HORIZONTAL;
        messageConstraint.weightx = 0.3;
    }

    public VariableAssignmentManager(final Container container, final Collection<SessionVariable> variables, final SessionVariableConsumer consumer) {
        _variables = Lists.newArrayListWithExpectedSize(variables.size());
        for (final SessionVariable variable : variables) {
            _variables.add(new VariableRow(variable, container, consumer));
        }
        logger.trace("managing variables {}", variables);
    }

    public void disableEditors() {
        for (final VariableRow variable : _variables) {
            variable.disable();
        }
    }

    private final class VariableRow implements ValueListener {
        private final SessionVariable         _variable;
        private final SessionVariableConsumer _consumer;
        private final JLabel                  _message;
        private final Component               _editor;

        VariableRow(final SessionVariable variable, final Container container, final SessionVariableConsumer consumer) {
            _variable = variable;
            _consumer = consumer;
            _message = new JLabel(MESSAGE_SPACE);

            if (!_variable.isHidden()) {
                container.add(new JLabel(_variable.getLabel()), labelConstraint);
                final Component display;
                if (_variable.isMutable()) {
                    _editor = display = _variable.getEditor();
                } else {
                    display = new JLabel(_variable.getValue().asString());
                    _editor = null;
                }
                container.add(display, valueConstraint);
                container.add(_message, messageConstraint);
            } else {
                _editor = null;
            }

            _variable.addListener(this);
            _variable.refresh();

            try {
                final Value value = _variable.getValue();
                _variable.validate(value == null ? "" : value.asString());
                consumer.update(_variable, true);
            } catch (InvalidValueException e) {
                consumer.update(_variable, false);
            }
        }

        /**
         * {@inheritDoc}
         */
        public void hasChanged(final SessionVariable variable) {
            logger.trace("{} has changed", variable);
            assert _variable.getName().equals(variable.getName());
            _message.setText(null);
            _message.setToolTipText(null);
            final Container parent = _message.getParent();
            if (null != parent) {
                parent.validate();
            }
            _consumer.update(variable, true);

            final Iterator<VariableRow> vi = _variables.iterator();
            while (vi.hasNext()) {
                final VariableRow next = vi.next();
                if (equals(next)) {
                    break;
                }
            }

            while (vi.hasNext()) {
                final VariableRow next = vi.next();
                next._variable.refresh();
            }
        }

        /**
         * {@inheritDoc}
         */
        public void isInvalid(final SessionVariable variable, final Object value, final String message) {
            _consumer.update(_variable, false);
            _message.setText(MESSAGE_INVALID);
            _message.setForeground(Color.RED);
            _message.setToolTipText(message);
            _message.getParent().validate();
        }

        public void disable() {
            if (_editor != null) {
                _editor.setEnabled(false);
            }
        }
    }

    private static final String MESSAGE_SPACE   = "                ";
    private static final String MESSAGE_INVALID = "INVALID";

    private static final Logger logger = LoggerFactory.getLogger(VariableAssignmentManager.class);

    private final List<VariableRow> _variables;
}
