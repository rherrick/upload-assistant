/*
 * upload-assistant: org.nrg.xnat.upload.ui.UploadWizardResultProducer
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import org.netbeans.spi.wizard.DeferredWizardResult;
import org.netbeans.spi.wizard.ResultProgressHandle;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;
import org.nrg.xnat.upload.application.UploadAssistant;
import org.nrg.xnat.upload.data.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static org.nrg.xnat.upload.application.UploadAssistant.getSettings;

public class UploadWizardResultProducer implements WizardResultProducer {
    public UploadWizardResultProducer(final UploadAssistant uploadAssistant, final ExecutorService executorService) {
        _uploadAssistant = uploadAssistant;
        _executorService = executorService;
    }

    /* (non-Javadoc)
     * @see org.netbeans.spi.wizard.WizardPage.WizardResultProducer#cancel(java.util.Map)
     */
    @SuppressWarnings("rawtypes")
    public boolean cancel(final Map arg0) { return true; }

    /* (non-Javadoc)
     * @see org.netbeans.spi.wizard.WizardPage.WizardResultProducer#finish(java.util.Map)
     */
    @SuppressWarnings("rawtypes")
    public Object finish(final Map arg0){
        return new UploadWizardResult();
    }

    private class UploadWizardResult extends DeferredWizardResult {
        private final Logger logger = LoggerFactory.getLogger(UploadWizardResult.class);

        private Future<Boolean> upload = null;

        UploadWizardResult() {
            super(true);
        }

        /*
         * (non-Javadoc)
         * @see org.netbeans.spi.wizard.DeferredWizardResult#start(java.util.Map, org.netbeans.spi.wizard.ResultProgressHandle)
         */
        @SuppressWarnings("rawtypes")
        @Override
        public void start(final Map wizardData, final ResultProgressHandle progress) {
            try {
                final Session session = getSettings().getCurrentSession();
                logger.trace("Wizard parameter map {}", wizardData);
                upload = _executorService.submit(new Callable<Boolean>() {
                    public Boolean call() {
                        return session.uploadTo(wizardData, new SwingUploadFailureHandler(), progress);
                    }
                });
                upload.get();
            } catch (final CancellationException ignored) {
                progress.failed("Upload canceled", false);
            } catch (final Throwable throwable) {
                System.out.println("upload failed: " + throwable);
                throwable.printStackTrace();
                logger.error("Something went wrong during upload", throwable);
                progress.failed(throwable.getMessage(), false);
            } finally {
                _uploadAssistant.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            }
        }

        /*
         * (non-Javadoc)
         * @see org.netbeans.spi.wizard.DeferredWizardResult#abort()
         */
        @Override
        public void abort() {
            if (null != upload) { upload.cancel(true); }
        }
    }

    private final UploadAssistant _uploadAssistant;
    private final ExecutorService _executorService;
}
