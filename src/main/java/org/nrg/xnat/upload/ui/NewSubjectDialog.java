/*
 * upload-assistant: org.nrg.xnat.upload.ui.NewSubjectDialog
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.ui.TraceableGridBagPanel;
import org.nrg.xnat.Labels;
import org.nrg.xnat.upload.data.Project;
import org.nrg.xnat.upload.data.Subject;
import org.nrg.xnat.upload.data.SubjectInformation;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Callable;

public class NewSubjectDialog extends AssistantDialog {

    public NewSubjectDialog(final SelectSubjectPage page, final Project project) {
        super(UIUtils.findParentFrame(page), Messages.getMessage("newsubjectdialog.title"), true);
        setLocationRelativeTo(getOwner());

        _page = page;
        _project = project;
        _contents = new TraceableGridBagPanel();
        _subjectId = makeTextField();

        _cancel = Messages.getMessage("button.cancel");

        setContentPane(getContents());
        pack();
    }

    public void reset() {
        _subjectId.setText("");
    }

    private void doCreateSubject(final String label) {
        final Callable<Subject> doCreate = new Callable<Subject>() {
            public Subject call() throws SubjectInformation.UploadSubjectException {
                final SubjectInformation subjectInfo = new SubjectInformation(_project);
                subjectInfo.setLabel(label);
                try {
                    final Subject subject = subjectInfo.uploadTo();
                    _project.addSubject(subject);
                    _page.refreshSubjectList(subject);
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            NewSubjectDialog.this.dispose();
                        }
                    });
                    return subject;
                } catch (SubjectInformation.UploadSubjectException e) {
                    _log.error("Unable to create subject " + label, e);
                    JOptionPane.showMessageDialog(NewSubjectDialog.this,
                                                  Messages.getDialogFormattedMessage("newsubjectdialog.error.message", label, e.getMessage()),
                                                  Messages.getMessage("newsubjectdialog.error.title"),
                                                  JOptionPane.ERROR_MESSAGE);
                    throw e;
                }
            }
        };
        _project.submit(doCreate);
    }

    private JPanel getContents() {
        final JLabel idLabel   = makeLabel(Messages.getMessage("newsubjectdialog.label"));
        final JLabel idMessage = makeMessage(" ");

        // Start with empty label text, so create button must be disabled
        idMessage.setText(Messages.getMessage("newsubjectdialog.message.empty"));
        idMessage.setForeground(Color.RED);

        final JButton cancelButton = new JButton(_cancel);
        final JButton createButton = new JButton(Messages.getMessage("newsubjectdialog.button.create"));
        createButton.setEnabled(false);

        final ActionListener createListener = new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                final String label = _subjectId.getText();
                if (Labels.isValidLabel(label)) {
                    createButton.setEnabled(false);
                    cancelButton.setEnabled(false);
                    doCreateSubject(label);
                } else {
                    // This should actually already be true, but some impls, e.g. Mac OS X, allow you to click button anyways.
                    idMessage.setText(Messages.getMessage("newsubjectdialog.message.invalid"));
                    createButton.setEnabled(false);
                }
            }
        };

        createButton.addActionListener(createListener);

        _subjectId.addActionListener(createListener);
        _subjectId.getDocument().addDocumentListener(new DocumentListener() {
            private void handle(final DocumentEvent event) {
                if (_log.isDebugEnabled()) {
                    _log.debug("Handling event " + event.getType().toString());
                }

                // If the subject dialog is re-used, cancel will be disabled, so this just makes sure it's enabled.
                cancelButton.setEnabled(true);

                // Get the submitted subject ID and test for validity.
                final String subjectId = _subjectId.getText();
                if (Strings.isNullOrEmpty(subjectId)) {
                    idMessage.setText(Messages.getMessage("newsubjectdialog.message.empty"));
                    createButton.setEnabled(false);
                } else if (!Labels.isValidLabel(subjectId)) {
                    idMessage.setText(Messages.getMessage("newsubjectdialog.message.invalid"));
                    createButton.setEnabled(false);
                } else {
                    try {
                        if (_project.hasSubject(subjectId)) {
                            idMessage.setText(Messages.getMessage("newsubjectdialog.message.exists", subjectId));
                            createButton.setEnabled(false);
                        } else {
                            idMessage.setText("");
                            createButton.setEnabled(true);
                        }
                    } catch (Throwable e) {
                        _log.error("unable to check for subject " + subjectId + " in " + _project, e);
                        idMessage.setText("");
                        createButton.setEnabled(true);
                    }
                }
            }

            public void removeUpdate(DocumentEvent event) {
                handle(event);
            }

            public void insertUpdate(DocumentEvent event) {
                handle(event);
            }

            public void changedUpdate(DocumentEvent event) {
                handle(event);
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                if (StringUtils.equals(_cancel, event.getActionCommand())) {
                    NewSubjectDialog.this.dispose();
                }
            }
        });

        _contents.add(idLabel, makeLabelConstraints(0));
        _contents.add(_subjectId, makeValueConstraints(0));
        _contents.add(idMessage, makeMessageConstraints(1));
        _contents.add(cancelButton, makeButtonConstraints(1, 2));
        _contents.add(createButton, makeButtonConstraints(2, 2));

        return _contents;
    }

    private static final Logger _log = LoggerFactory.getLogger(NewSubjectDialog.class);

    private final SelectSubjectPage _page;
    private final Project           _project;
    private final JPanel            _contents;
    private final JTextField        _subjectId;
    private final String            _cancel;
}
