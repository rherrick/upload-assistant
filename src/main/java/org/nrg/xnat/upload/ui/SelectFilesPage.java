/*
 * upload-assistant: org.nrg.xnat.upload.ui.SelectFilesPage
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;
import org.nrg.xnat.upload.util.Messages;

import javax.swing.*;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import static org.nrg.xnat.upload.application.UploadAssistant.getSettings;

public class SelectFilesPage extends WizardPage {
    private final JFileChooser _chooser;

    public static String getDescription() {
        return Messages.getPageTitle(SelectFilesPage.class);
    }

    public SelectFilesPage() {
        _chooser = new JFileChooser();
        _chooser.setControlButtonsAreShown(false);
        _chooser.setMultiSelectionEnabled(true);
        _chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        _chooser.setAcceptAllFileFilterUsed(false);
        _chooser.setFileHidingEnabled(true);
        final File currentDirectory = getSettings().getCurrentDirectory();
        if (currentDirectory != null) {
            _chooser.setCurrentDirectory(currentDirectory);
        }
        setLongDescription(Messages.getPageDescription(SelectFilesPage.class));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void recycle() {
        removeAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void renderingPage() {
        add(_chooser);
    }

    /**
     * Indicates whether the user can proceed to the next page. For this page, the current directory in the file chooser
     * is set as the current directory in preferences to be reused for future operations. There are no checks that block
     * the user's ability to proceed.
     *
     * @param stepName The name of the current step.
     * @param settings Any settings for the current step.
     * @param wizard   The current wizard.
     *
     * @return Returns <b>WizardPanelNavResult.PROCEED</b> in all cases.
     */
    @Override
    public WizardPanelNavResult allowNext(final String stepName, final Map settings, final Wizard wizard) {
        getSettings().setCurrentDirectory(_chooser.getCurrentDirectory());

        final File[] selected = _chooser.getSelectedFiles();
        getSettings().setSelectedFiles(selected.length > 0 ? Arrays.asList(selected) : Collections.singletonList(_chooser.getCurrentDirectory()));

        return WizardPanelNavResult.PROCEED;
    }
}
