/*
 * upload-assistant: org.nrg.xnat.upload.dcm.IndexedSessionLabelFunction
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.dcm;

import com.google.common.base.Function;
import org.nrg.dcm.edit.AbstractIndexedLabelFunction;
import org.nrg.dcm.edit.annotations.DicomEdit4Function;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.scripts.ScriptFunction;
import org.nrg.xnat.upload.data.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.nrg.xnat.upload.application.UploadAssistant.getCurrentXnatServer;
import static org.nrg.xnat.upload.data.SessionVariableNames.PROJECT;
import static org.nrg.xnat.upload.data.SessionVariableNames.SESSIONS;

@DicomEdit4Function(name = "makeSessionLabel")
public final class IndexedSessionLabelFunction extends AbstractIndexedLabelFunction implements ScriptFunction {
    /**
     * This sets up an indexed session label function for the submitted sessions.
     *
     * @param context The anonymization context.
     */
    public IndexedSessionLabelFunction(final Map<String, Object> context) {
        if (context.containsKey(SESSIONS)) {
            //noinspection unchecked
            _sessions = (Map<String, String>) context.get(SESSIONS);
        } else if (context.containsKey(PROJECT)) {
            final Object object = context.get(PROJECT);
            final Project project;
            if (object instanceof String) {
                project = getCurrentXnatServer().getProject((String) object);
            } else if (object instanceof Project) {
                project = (Project) object;
            } else {
                throw new RuntimeException("Found an object of type " + object.getClass().getName() + " stored as \"" + PROJECT + "\". This must be a string or project instance.");
            }

            try {
                _sessions = project.getSessionLabels().toMap();
            } catch (InterruptedException | ExecutionException e) {
                _log.error("An error occurred trying to get the session labels for the project " + project.toString(), e);
                throw new RuntimeException(e);
            }
        } else {
            _log.warn("Couldn't find either a map of session IDs and labels or a current project object, setting filter to empty.");
            _sessions = Collections.emptyMap();
        }
    }

    /**
     * This returns a function that returns true in the case where the session label retrieval failed; this is sort of
     * broken but prevents an ugly infinite loop situation.
     *
     * @return Returns the function to test for availability.
     */
    @Override
    protected Function<String, Boolean> isAvailable() throws ScriptEvaluationException {
        return new Function<String, Boolean>() {
            @Override
            public Boolean apply(final String label) {
                return !_sessions.containsKey(label) && !_sessions.containsValue(label);
            }
        };
    }

    private static final Logger _log = LoggerFactory.getLogger(IndexedSessionLabelFunction.class);

    private final Map<String, String> _sessions;
}
