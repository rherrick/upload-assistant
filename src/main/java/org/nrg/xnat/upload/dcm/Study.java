/*
 * upload-assistant: org.nrg.xnat.upload.dcm.Study
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.dcm;

import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.*;
import org.apache.commons.lang3.StringUtils;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.json.JSONException;
import org.netbeans.spi.wizard.ResultProgressHandle;
import org.netbeans.spi.wizard.Summary;
import org.nrg.dicom.mizer.exceptions.MizerContextException;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.service.MizerContext;
import org.nrg.dicom.mizer.service.MizerService;
import org.nrg.dicom.mizer.service.impl.MizerContextWithScript;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.variables.Variable;
import org.nrg.framework.concurrency.LoggingThreadPoolExecutor;
import org.nrg.framework.constants.PrearchiveCode;
import org.nrg.xnat.upload.application.UploadAssistantSettings;
import org.nrg.xnat.upload.data.*;
import org.nrg.xnat.upload.io.HttpUploadException;
import org.nrg.xnat.upload.io.UploadStatisticsReporter;
import org.nrg.xnat.upload.io.dcm.ZipSeriesUploader;
import org.nrg.xnat.upload.net.HttpException;
import org.nrg.xnat.upload.net.JSONRequestConnectionProcessor;
import org.nrg.xnat.upload.net.XnatServer;
import org.nrg.xnat.upload.ui.UploadResultPanel;
import org.nrg.xnat.upload.util.MapRegistry;
import org.nrg.xnat.upload.util.Messages;
import org.nrg.xnat.upload.util.Registry;
import org.nrg.xnat.upload.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ChoiceFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;

import static org.nrg.xnat.upload.application.UploadAssistant.*;
import static org.nrg.xnat.upload.data.SessionVariableNames.*;
import static org.nrg.xnat.upload.ui.UIUtils.buildCommitEntity;

public class Study extends MapEntity implements Entity, Session {

    public static final int MAX_TAG = Collections.max(new ArrayList<Integer>() {{
        add(Tag.AccessionNumber);
        add(Tag.StudyDate);
        add(Tag.StudyDescription);
        add(Tag.StudyID);
        add(Tag.StudyInstanceUID);
        add(Tag.StudyTime);
    }});

    public static final String PREVENT_ANON        = "prevent_anon";
    public static final String PREVENT_AUTO_COMMIT = "prevent_auto_commit";
    public static final String SOURCE              = "SOURCE";

    private static final Logger logger = LoggerFactory.getLogger(Study.class);

    private final Registry<Series> _seriesRegistry = new MapRegistry<>(new TreeMap<Series, Series>());
    private final Date                           dateTime;
    private final XnatServer                     _server;
    private final Future<MizerContextWithScript> _siteWideScript;

    public Study(final String uid, final Date dateTime, final String id, final String accessionNumber, final String description) {
        put(Tag.StudyInstanceUID, uid);
        this.dateTime = dateTime;
        if (null != dateTime) {
            put(Tag.StudyDate, new SimpleDateFormat("yyyyMMdd").format(dateTime));
            put(Tag.StudyTime, new SimpleDateFormat("HHmmss").format(dateTime));
        }
        put(Tag.StudyID, id);
        put(Tag.AccessionNumber, accessionNumber);
        put(Tag.StudyDescription, description);

        _server = getCurrentXnatServer();
        _siteWideScript = _server.getSiteWideAnonScript();
    }

    public Study(final DicomObject o) {
        this(o.getString(Tag.StudyInstanceUID),
             Utils.getDateTime(o, Tag.StudyDate, Tag.StudyTime),
             o.getString(Tag.StudyID),
             o.getString(Tag.AccessionNumber),
             o.getString(Tag.StudyDescription));
    }

    public Study(final DicomObject o, final String suggestedModality) {
        this(o.getString(Tag.StudyInstanceUID),
             Utils.getDateTime(o, Tag.StudyDate, Tag.StudyTime),
             o.getString(Tag.StudyID),
             o.getString(Tag.AccessionNumber),
             o.getString(Tag.StudyDescription));
        put(Tag.Modality, suggestedModality);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getAccession()
     */
    public String getAccession() {
        return (String) get(Tag.AccessionNumber);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getDateTime()
     */
    public Date getDateTime() {
        return dateTime;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getTimeZone()
     */
    public TimeZone getTimeZone() {
        // DICOM does not store timezone information so return null
        return null;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getDescription()
     */
    public String getDescription() {
        return (String) get(Tag.StudyDescription);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getFileCount()
     */
    public int getFileCount() {
        int count = 0;
        for (final Series s : _seriesRegistry) {
            count += s.getFileCount();
        }
        return count;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getFormat()
     */
    public String getFormat() {
        return "DICOM";
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getID()
     */
    public String getID() {
        return (String) get(Tag.StudyID);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getModalities()
     */
    public Set<String> getModalities() {
        final Set<String> modalities = Sets.newLinkedHashSet();
        for (final Series s : _seriesRegistry) {
            modalities.addAll(s.getModalities());
        }
        return modalities;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getScanCount()
     */
    public int getScanCount() {
        return _seriesRegistry.size();
    }

    @SuppressWarnings("UnusedReturnValue")
    public Series getSeries(final DicomObject object, final File file) {
        final Series series = _seriesRegistry.get(new Series(this, object));
        series.addFile(file, object);
        return series;
    }

    /*
     * (non-Javadoc)
     * @see Entity#getSeriesRegistry()
     */
    public Collection<Series> getSeriesRegistry() {
        return _seriesRegistry.getAll();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getSize()
     */
    public long getSize() {
        long size = 0;
        for (final Series s : _seriesRegistry) {
            size += s.getSize();
        }
        return size;
    }

    /*
     * (non-Javadoc)
     * @see Entity#getStudies()
     */
    public Collection<Study> getStudies() {
        return Collections.singleton(this);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof Study)) {
            return false;
        }
        if (o == this) {
            return true;
        }

        final Study  that                 = (Study) o;
        final Object thisStudyInstanceUid = get(Tag.StudyInstanceUID);
        final Object thatStudyInstanceUid = that.get(Tag.StudyInstanceUID);

        if (!com.google.common.base.Objects.equal(thisStudyInstanceUid, thatStudyInstanceUid)) {
            return false;
        }

        final Object thisModality = get(Tag.Modality);
        if (thisModality != null) {
            final Object thatModality = that.get(Tag.Modality);
            return com.google.common.base.Objects.equal(thisModality, thatModality);
        } else {
            return true;
        }
    }

    /*
     * (non-Javadoc)
     * @see MapEntity#hashCode()
     */
    @Override
    public int hashCode() {
        final Object modality = get(Tag.Modality);
        return modality == null
               ? com.google.common.base.Objects.hashCode(get(Tag.StudyInstanceUID))
               : com.google.common.base.Objects.hashCode(get(Tag.StudyInstanceUID), get(Tag.Modality));
    }

    /**
     * Provides a study identifier that is as unique and verbose as possible.
     *
     * @return The study identifier.
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder  = new StringBuilder("DICOM ");
        final Object        modality = get(Tag.Modality);
        if (modality != null) {
            builder.append(modality).append(" ");
        }
        builder.append("study ");
        final Object studyId = get(Tag.StudyID);
        builder.append(studyId);
        final Object accessionNumber = get(Tag.AccessionNumber);
        if (null != accessionNumber) {
            builder.append(" (").append(accessionNumber).append(")");
        }
        final Object description = get(Tag.StudyDescription);
        if (null != description) {
            builder.append(" ").append(description);
        }
        if (null == studyId && null == accessionNumber) {
            builder.append(" [").append(get(Tag.StudyInstanceUID)).append("]");
        }
        return builder.toString();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#uploadTo(java.util.Map, org.nrg.upload.data.UploadFailureHandler, org.netbeans.spi.wizard.ResultProgressHandle)
     */
    @SuppressWarnings("ConstantConditions")
    public boolean uploadTo(final Map<?, ?> parameters, final UploadFailureHandler failureHandler, final ResultProgressHandle progress) {
        final long uploadStart = new Date().getTime();
        try {
            int fileCount = getFileCount();
            progress.setProgress(0, fileCount);
            progress.setBusy("Building session manifest");

            final List<Series> uploads = Lists.newArrayList(Iterables.filter(_seriesRegistry, new Predicate<Series>() {
                public boolean apply(final Series series) {
                    return series.isUploadAllowed();
                }
            }));
            if (uploads.isEmpty()) {
                progress.failed("No files were selected for upload", true);
                return false;
            }

            final UploadAssistantSettings settings             = getSettings();
            final Project                 project              = getCurrentXnatServer().getCurrentProject();
            final Subject                 subject              = settings.getCurrentSubject();
            final SessionVariable         sessionLabelVariable = settings.getSessionLabel();

            final Value sessionLabelValue;
            if (sessionLabelVariable == null) {
                sessionLabelValue = null;
                logger.warn("No value was set for the session label. This is probably not what you intended.");
            } else {
                sessionLabelValue = sessionLabelVariable.getValue();
            }

            final String session;
            if (sessionLabelValue == null) {
                session = null;
                logger.warn("No value was set for the session label. This is probably not what you intended.");
            } else {
                session = sessionLabelValue.asString();
            }

            final Map<String, SessionVariable> variables = settings.getSessionVariables();
            final String                       visit     = variables.containsKey(VISIT_LABEL) ? variables.get(VISIT_LABEL).getValue().asString() : null;
            final String                       protocol  = variables.containsKey(PROTOCOL_LABEL) ? variables.get(PROTOCOL_LABEL).getValue().asString() : null;

            final Properties properties = new Properties();
            properties.setProperty(PROJECT, project.toString());
            properties.setProperty(SUBJECT, subject.getLabel());
            properties.setProperty(SESSION, StringUtils.defaultIfBlank(session, ""));

            // Build the URL for POSTing zips
            final URL baseURL = _server.getValidatedUrl();
            final URL dataPostURL;
            try {
                final StringBuilder buffer = new StringBuilder(baseURL.toString());
                buffer.append("/data/services/import?import-handler=DICOM-zip");
                buffer.append("&PROJECT_ID=").append(project);
                buffer.append("&SUBJECT_ID=").append(subject);
                if (StringUtils.isNotBlank(session)) {
                    buffer.append("&EXPT_LABEL=").append(session);
                } else {
                    logger.warn("No value was set for the session label. This is probably not what you intended.");
                }
                if (StringUtils.isNotBlank(visit)) {
                    buffer.append("&VISIT=").append(visit);
                    properties.setProperty(VISIT_LABEL, visit);
                }
                if (StringUtils.isNotBlank(protocol)) {
                    buffer.append("&PROTOCOL=").append(protocol);
                    properties.setProperty(PROTOCOL_LABEL, visit);
                }
                buffer.append("&rename=true&" + PREVENT_ANON + "=true&" + PREVENT_AUTO_COMMIT + "=true&" + SOURCE + "=uploader");

                // TODO I'm not sure that this part will work at all. Check compatibility with Importer query variables.
                final PrearchiveCode prearchiveCode = getPrearchiveCode(project);
                if (prearchiveCode != null) {
                    buffer.append("&autoArchive=").append(prearchiveCode);
                }
                dataPostURL = new URL(buffer.toString());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            // Get any session variables that we haven't already set.
            for (final String name : variables.keySet()) {
                if (!StringUtils.equalsAny(name, PROJECT, SUBJECT, SESSION, VISIT_LABEL, PROTOCOL_LABEL)) {
                    final Value value = variables.get(name).getValue();
                    properties.setProperty(name, value != null ? StringUtils.defaultIfBlank(value.asString(), "") : "");
                }
            }

            final int nThreads = settings.getUploadThreads();

            progress.setBusy("Preparing upload...");
            logger.trace("creating thread pool and executors");
            final ExecutorService                             executor          = LoggingThreadPoolExecutor.newFixedThreadPool(nThreads);
            final CompletionService<Set<String>>              completionService = new ExecutorCompletionService<>(executor);
            final Map<Future<Set<String>>, ZipSeriesUploader> uploaders         = Maps.newHashMap();
            logger.trace("submitting uploaders for {}", uploads);
            final UploadStatisticsReporter stats = new UploadStatisticsReporter(progress);

            final MizerContext siteWide = getSiteWideAnonScript();

            for (final Series series : uploads) {
                stats.addToSend(series.getSize());
                final List<MizerContext> scripts = new ArrayList<>();
                if (siteWide != null) {
                    siteWide.add(properties);
                    try {
                        siteWide.setElement(SESSIONS, project.getSessionLabels().toMap());
                    } catch (InterruptedException | ExecutionException e) {
                        logger.warn("An error occurred trying to retrieve the session labels for the project " + project.toString(), e);
                    }
                    scripts.add(siteWide);
                }
                final MizerContext projectSpecific = project.getProjectAnonScript();
                if (projectSpecific != null) {
                    projectSpecific.add(properties);
                    scripts.add(projectSpecific);
                }
                final ZipSeriesUploader uploader = new ZipSeriesUploader(dataPostURL, series, scripts, stats);
                uploaders.put(completionService.submit(uploader), uploader);
            }

            final Set<String>            uris     = Sets.newLinkedHashSet();
            final Map<Series, Throwable> failures = Maps.newLinkedHashMap();
            while (progress.isRunning() && !uploaders.isEmpty()) {
                final Future<Set<String>> future;
                try {
                    future = completionService.take();
                    logger.trace("retrieved completed future {}", future);
                } catch (InterruptedException e) {
                    logger.debug("upload completion poll interrupted", e);
                    continue;
                }
                try {
                    final Set<String> us = future.get();
                    logger.debug("{} completed -> {}", uploaders.get(future), us);
                    uris.addAll(us);
                    uploaders.remove(future);
                } catch (InterruptedException e) {
                    logger.info("upload interrupted or timed out, retrying");
                    completionService.submit(uploaders.get(future));
                } catch (ExecutionException exception) {
                    executor.shutdownNow();
                    final Throwable cause = exception.getCause();
                    //noinspection ThrowableResultOfMethodCallIgnored
                    failures.put(uploaders.remove(future).getSeries(), cause);
                    future.cancel(true);
                    final UploadAbortedException aborted;
                    if (cause instanceof UploadAbortedException) {
                        aborted = (UploadAbortedException) cause;
                    } else {
                        aborted = new UploadAbortedException(cause);
                    }
                    logger.info("upload aborted: shutting down executor", cause);

                    for (final Map.Entry<Future<Set<String>>, ZipSeriesUploader> me : uploaders.entrySet()) {
                        me.getKey().cancel(true);
                        //noinspection ThrowableResultOfMethodCallIgnored
                        failures.put(me.getValue().getSeries(), aborted);
                    }
                    String message = buildFailureMessage(failures);
                    progress.failed(message, false);
                    return false;
                }
            }

            if (!uploaders.isEmpty()) {
                logger.error("progress failed before uploaders complete: {}", uploaders);
                return false;
            } else if (1 == uris.size()) {
                final String uri = uris.iterator().next();
                try {
                    final URL url = new URL(baseURL.toString() + uri);
                    return closeSession(url, session, parameters, progress, failures);
                } catch (MalformedURLException e) {
                    progress.failed("<p>The XNAT server provided an invalid response</p>" + "<p>" + uri + "</p>" + "<p>Please contact your XNAT system administrators for help.</p>", false);
                    return false;
                }
            } else {
                logger.error("Server reports unexpected session count {}: {}", uris.size(), uris);
                progress.failed("<p>The XNAT server reported receiving an unexpected number of sessions: (" + uris.size() + ")</p>" + "<p>Please contact your XNAT system administrators for help.</p>", false);
                return false;
            }
        } finally {
            long duration = (new Date().getTime() - uploadStart) / 1000;
            logger.info("upload operation complete after {} sec", duration);
        }
    }

    /**
     * The RestServer URL includes the web application part of the path.
     * If the given path starts with the web application path, returns the path
     * minus the web application context; otherwise return the full path. Either
     * way, any leading /'s are removed.
     *
     * @param url  The URL to be inspected.
     * @param path The relative path to validate.
     *
     * @return The relative path to the REST server URL, stripped of leading slashes.
     */
    public static String getWebAppRelativePath(final URL url, final String path) {
        final StringBuilder buffer = new StringBuilder(path);
        while ('/' == buffer.charAt(0)) {
            buffer.deleteCharAt(0);
        }
        final String context        = url.getPath();
        boolean      pathHasContext = true;
        for (int i = 0; i < context.length(); i++) {
            if (context.charAt(i) != path.charAt(i)) {
                pathHasContext = false;
                break;
            }
        }
        if (pathHasContext) {
            buffer.delete(0, context.length());
        }
        while ('/' == buffer.charAt(0)) {
            buffer.deleteCharAt(0);
        }
        return buffer.toString();
    }

    public static URL buildSessionViewURL(final URL url, final String relativePath) {
        final String[] components = relativePath.split("/");
        if (!"data".equals(components[0]) && !"REST".equals(components[0])) {
            Study.logger.warn("Strange session path {}: first component is neither \"data\" nor \"REST\"", relativePath);
        }
        if ("prearchive".equals(components[1])) {
            // prearchive sessions need some extra help for nice display
            try {
                return new URL(url.toString() + "?screen=XDATScreen_uploaded_xnat_imageSessionData.vm");
            } catch (MalformedURLException e) {
                Study.logger.error("can't build prearchive session view url for " + url, e);
                return url;
            }
        } else {
            // archived sessions are viewable using REST url
            return url;
        }
    }

    public static String formatServer(final URL url) {
        final StringBuilder buffer = new StringBuilder(url.getProtocol());
        buffer.append("://").append(url.getHost());
        final int httpPort = url.getPort();
        if (-1 != httpPort && httpPort != url.getDefaultPort()) {
            buffer.append(":").append(httpPort);
        }
        return buffer.toString();
    }

    public static <T> String buildFailureMessage(final Map<?, T> failures) {
        final StringBuilder       buffer  = new StringBuilder("<html>");
        final Multimap<T, Object> inverse = LinkedHashMultimap.create();

        Multimaps.invertFrom(Multimaps.forMap(failures), inverse);

        final Multimap<Object, ?> causes = Utils.consolidateKeys(inverse, 4);
        final MessageFormat       format = new MessageFormat("{0} not uploaded: {1}");

        format.setFormatByArgumentIndex(0, new ChoiceFormat(new double[]{0, 1, 2},
                                                            new String[]{"No items", "One item", "{0,number} items"}));
        for (final Object key : causes.keySet()) {
            final Collection<?> items = causes.get(key);
            final Object        message;
            if (key instanceof HttpUploadException) {
                message = key.toString();
            } else {
                message = key;
            }
            buffer.append("<p>").append(format.format(new Object[]{items.size(), message}));
            buffer.append("</p><br>");
        }
        return buffer.append("</html>").toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SessionVariable> getVariables(final Project project) {
        return Collections.emptyList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SessionVariable> getVariables(final List<MizerContext> contexts) {
        final MizerService  service   = getMizerService();
        final Set<Variable> variables = Sets.newLinkedHashSet();
        try {
            variables.addAll(service.getReferencedVariables(contexts));
        } catch (MizerException e) {
            if (e instanceof MizerContextException) {
                final MizerContext context = ((MizerContextException) e).getContext();
                logger.error("An error occurred processing an anonymization script: " + context.toString(), e);
            } else {
                logger.error("An error occurred retrieving variables from anonymization scripts", e);
            }
        }

        final DicomObject           object           = _seriesRegistry.isEmpty() ? null : _seriesRegistry.get(0).getSampleObject();
        final List<SessionVariable> sessionVariables = Lists.newArrayList();
        for (final Variable variable : variables) {
            sessionVariables.add(DicomSessionVariable.getSessionVariable(variable, object));
        }
        return sessionVariables;
    }

    private MizerContext getSiteWideAnonScript() {
        try {
            return _siteWideScript.get();
        } catch (InterruptedException | ExecutionException e) {
            logger.error("An error occurred trying to retrieve the site-wide anon script. Uploading can't continue, since this might introduce PHI to the system.", e);
            throw new RuntimeException(e);
        }
    }

    private boolean closeSession(final URL url, final String session, final Map<?, ?> params, final ResultProgressHandle progress, final Map<?, ?> failures) {
        // Close session and return result
        try {
            if (failures.isEmpty()) {
                progress.setBusy("Committing session");
                logger.trace("committing session {}", url);
                final JSONRequestConnectionProcessor handler = new JSONRequestConnectionProcessor(buildCommitEntity());

                String queryParams = "?action=commit&SOURCE=uploader";
                //add visit
                if (null != params.get(VISIT_LABEL) && !Strings.isNullOrEmpty(((AssignedSessionVariable) params.get(VISIT_LABEL)).getValue().asString())) {
                    queryParams += "&VISIT=" + ((AssignedSessionVariable) params.get(VISIT_LABEL)).getValue().asString();
                }
                //add protocol
                if (null != params.get(PROTOCOL_LABEL) && !Strings.isNullOrEmpty(((AssignedSessionVariable) params.get(PROTOCOL_LABEL)).getValue().asString())) {
                    queryParams += "&PROTOCOL=" + ((AssignedSessionVariable) params.get(PROTOCOL_LABEL)).getValue().asString();
                }

                final URL baseURL = getCurrentXnatServer().getValidatedUrl();
                getRestServer().doPost(getWebAppRelativePath(baseURL, url.getPath()) + queryParams, handler);

                String response   = handler.getResponseEntity();
                String resultPath = getWebAppRelativePath(baseURL, response);

                final URL result = new URL(baseURL + "/" + resultPath);

                // TODO: build summary, notify user
                final UploadResultPanel resultPanel = new UploadResultPanel(session, buildSessionViewURL(result, resultPath));
                progress.finished(Summary.create(resultPanel, url));
                return true;
            } else {
                progress.failed(buildFailureMessage(failures), false);
                return false;
            }
        } catch (JSONException e) {
            logger.error("unable to write commit request entity", e);
            return false;
        } catch (HttpException e) {
            logger.error("session commit failed", e);
            switch (e.getResponseCode()) {
                case HttpURLConnection.HTTP_NOT_FOUND: {
                    progress.failed(Messages.getMessage("error.http.notfound", formatServer(url), url.getPath()), true);
                    return false;
                }

                case HttpURLConnection.HTTP_INTERNAL_ERROR: {
                    progress.failed(Messages.getMessage("error.http.internal", formatServer(url)), true);
                    return false;
                }

                case HttpURLConnection.HTTP_CONFLICT: {
                    progress.failed(Messages.getMessage("error.http.conflict", formatServer(url)), true);
                    return false;
                }

                default: {
                    progress.failed(Messages.getMessage("error.http.unknown", e.getResponseCode(), e.getMessage()), true);
                    return false;
                }
            }
        } catch (IOException e) {
            logger.error("Session commit failed", e);
            progress.failed(Messages.getMessage("error.io.exception", formatServer(url), e.getMessage()), false);
            return false;
        } catch (Throwable t) {
            logger.error("Session commit failed", t);
            progress.failed(Messages.getMessage("error.total.failure", t.getMessage()), false);
            return false;
        }
    }

    private PrearchiveCode getPrearchiveCode(final Project project) throws ExecutionException, InterruptedException {
        final PrearchiveCode prearchiveCode = getSettings().getPrearchiveCodeOverride();
        return prearchiveCode != null ? prearchiveCode : project.getPrearchiveCode();
    }
}
