/*
 * upload-assistant: org.nrg.xnat.upload.data.AbstractSessionVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.data;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.nrg.dicom.mizer.exceptions.MultipleInitializationException;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.variables.BasicVariable;
import org.nrg.dicom.mizer.variables.Variable;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.util.List;
import java.util.Set;

/**
 * Provides the base class for transporting variables extracted from anonymization scripts and a DICOM session.
 */
public abstract class AbstractSessionVariable implements SessionVariable {
    /**
     * Creates the variable from the submitted variable instance. In this case, this object works as a shim or wrapper
     * to make the standard Mizer variable work as a {@link SessionVariable}.
     *
     * @param variable The variable to wrap.
     */
    protected AbstractSessionVariable(final Variable variable) {
        _variable = variable;
    }

    /**
     * Creates the variable with the indicated name.
     *
     * @param name The name of the variable to create.
     */
    protected AbstractSessionVariable(final String name) {
        this(name, null);
    }

    /**
     * Creates the variable with the indicated name and export field.
     *
     * @param name        The name of the variable to create.
     * @param exportField The export field of the variable to create.
     */
    protected AbstractSessionVariable(final String name, final String exportField) {
        _variable = new BasicVariable(name);
        setExportField(exportField);
    }

    @Override
    public String getName() {
        return _variable.getName();
    }

    @Override
    public String getDescription() {
        return _variable.getDescription();
    }

    @Override
    public void setDescription(final String description) {
        _variable.setDescription(description);
    }

    @Override
    public Value getValue() {
        return _variable.getValue();
    }

    @Override
    public String setValue(final String value) throws InvalidValueException {
        return setValueInternal(value);
    }

    @Override
    public void setValue(final Value value) {
        _variable.setValue(value);
    }

    @Override
    public Value getInitialValue() {
        return _variable.getInitialValue();
    }

    @Override
    public void setInitialValue(final String initialValue) throws MultipleInitializationException {
        _variable.setInitialValue(initialValue);
    }

    @Override
    public void setInitialValue(final Value initialValue) throws MultipleInitializationException {
        _variable.setInitialValue(initialValue);
    }

    @Override
    public String getExportField() {
        return _variable.getExportField();
    }

    @Override
    public void setExportField(final String field) {
        _variable.setExportField(field);
    }

    @Override
    public boolean isHidden() {
        return _variable.isHidden();
    }

    @Override
    public void setIsHidden(final boolean hidden) {
        _variable.setIsHidden(hidden);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getValueMessage() {
        return _message;
    }

    /**
     * Gets a label for the session variable. This first tries to retrieve a localized version from the {@link Messages}
     * utility, looking for the value <b>variables.<i>{@link #getName()}</i></b>. If that's not found, it checks whether
     * the variable description has a valid value. Finally, it returns the variable name.
     *
     * Note that some variable names have asterisks added to disambiguate them from meaningful tokens in Mizer scripts.
     * These asterisks are stripped off so that, e.g., <b>*visit*</b> maps to the key <b>variables.visit</b>.
     *
     * @return The label for the variable.
     */
    @Override
    public String getLabel() {
        final String cleanName = clean(getName());
        final String key       = "variables." + cleanName;
        if (Messages.hasMessage(key)) {
            return Messages.getMessage(key);
        }
        if (StringUtils.isNotBlank(getDescription())) {
            return StringUtils.capitalize(getDescription());
        }
        return StringUtils.capitalize(cleanName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void addListener(final ValueListener listener) {
        if (!_listeners.contains(listener)) {
            _listeners.add(listener);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeListener(final ValueListener listener) {
        _listeners.remove(listener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void addValidator(final ValueValidator validator) {
        if (!_validators.contains(validator)) {
            _validators.add(validator);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMutable() {
        return _mutable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SessionVariable fixValue() {
        _mutable = false;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SessionVariable fixValue(final String value) throws InvalidValueException {
        setValue(value);
        return fixValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final String validate(final String value) throws InvalidValueException {
        final List<String> messages = Lists.newArrayList();
        for (final ValueValidator validator : _validators) {
            if (validator.isValid(value)) {
                final String message = validator.getMessage(value);
                if (StringUtils.isNotBlank(message)) {
                    messages.add(message);
                }
            } else {
                throw new InvalidValueException(validator.getMessage(value));
            }
        }
        return messages.isEmpty() ? null : Joiner.on("; ").join(messages);
    }

    @Override
    public void refresh() {

    }

    @Override
    public Component getEditor() {
        return null;
    }

    /**
     * Adds a "shadow" variable to this session variable.
     *
     * @param shadow The shadow to add.
     */
    @Override
    public final void addShadow(final SessionVariable shadow) {
        if (!shadows.contains(shadow)) {
            shadows.add(shadow);
        }
    }

    /**
     * Modifies the value of the internal variable to the submitted value. This doesn't try to update any UI components,
     * making it safe to use to handle value changes in UI components.
     *
     * @param value The value to set.
     */
    public void editTo(final String value) {
        try {
            _message = validate(value);
            setValueInternal(value);
            fireHasChanged();
        } catch (InvalidValueException e) {
            fireIsInvalid(value, e.getMessage());
        }
    }

    /**
     * Sets the value of the {@link #getEditor() variable editor} to the submitted string value. It then returns the
     * previous value contained in the text component. This method deactivates this instance's listener for the text
     * component, preventing annoying events telling you that you just updated the text.
     *
     * @return The previous value contained in the text component.
     */
    protected String updateDisplayValue() {
        final Component component = getEditor();
        if (!(component instanceof JTextComponent)) {
            return null;
        }

        final JTextComponent editor   = (JTextComponent) component;
        final String         current  = editor.getText();
        final Document       document = editor.getDocument();
        final Value          value    = getValue();

        synchronized (this) {
            document.removeDocumentListener((DocumentListener) this);
            logger.trace("Setting text field for " + getName() + " = " + value);
            editor.setText(value == null ? "" : value.asString());
            document.addDocumentListener((DocumentListener) this);
        }

        return current;
    }

    /**
     * Notify all listeners that the value of this variable has changed.
     */
    protected final void fireHasChanged() {
        final Value value = getValue();
        logger.trace("{} value has changed to {}", this, value);
        for (final ValueListener listener : _listeners) {
            logger.trace("{} notifying listener {} of value change", this, listener);
            listener.hasChanged(this);
        }
        for (final SessionVariable shadow : shadows) {
            try {
                shadow.setValue(value);
            } catch (InvalidValueException e) {
                logger.error(this + "unable to set shadow value", e);
            }
        }
    }

    /**
     * Notify all listeners that the user attempted to set this variable
     * to an invalid value
     *
     * @param value   the attempted invalid value
     * @param message explanation of what went wrong
     */
    protected final void fireIsInvalid(final Object value, final String message) {
        logger.trace("attempted to set {} to invalid value {}", this, value);
        for (final ValueListener listener : _listeners) {
            logger.trace("{} notifying listener {} of invalid value", this, listener);
            listener.isInvalid(this, value, message);
        }
    }

    /**
     * This provides direct access to set the value internally. This is necessary for use with the {@link
     * #editTo(String)} method, which can cause state errors by calling value methods that also update the text
     * components in the UI.
     *
     * @param value The value to set.
     *
     * @return The previously set value.
     */
    private String setValueInternal(final String value) {
        _message = validate(value);
        return _variable.setValue(value);
    }

    /**
     * Cleans asterisks from variable names.
     *
     * @param name The variable name to scrub.
     *
     * @return The cleaned name.
     */
    private String clean(final String name) {
        return StringUtils.isNotBlank(name) ? StringUtils.replaceAll(name, "\\*", "") : "";
    }

    private static final Logger logger = LoggerFactory.getLogger(AbstractSessionVariable.class);

    private final Set<ValueListener>   _listeners  = Sets.newLinkedHashSet();
    private final Set<ValueValidator>  _validators = Sets.newLinkedHashSet();
    private final Set<SessionVariable> shadows     = Sets.newLinkedHashSet();

    private boolean _mutable = true;

    private final Variable _variable;
    private       String   _message;
}
