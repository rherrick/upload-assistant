/*
 * upload-assistant: org.nrg.xnat.upload.data.Session
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.data;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.netbeans.spi.wizard.ResultProgressHandle;
import org.nrg.dicom.mizer.service.MizerContext;

public interface Session {
	String getID();
	String getAccession();
	Date getDateTime();
	String getDescription();
	int getScanCount();
	int getFileCount();
	long getSize();
	Set<String> getModalities();
	String getFormat();
	List<SessionVariable> getVariables(final List<MizerContext> contexts);
	List<SessionVariable> getVariables(final Project project);
	boolean uploadTo(Map<?,?> params, UploadFailureHandler handler, ResultProgressHandle progress);
	TimeZone getTimeZone();
}
