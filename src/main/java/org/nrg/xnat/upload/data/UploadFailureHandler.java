/*
 * upload-assistant: org.nrg.xnat.upload.data.UploadFailureHandler
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.data;

public interface UploadFailureHandler {
    boolean shouldRetry(Object item, Object cause);
}
