/*
 * org.nrg.upload.data.Project
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/11/14 4:28 PM
 */
package org.nrg.xnat.upload.data;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.nrg.dicom.mizer.scripts.ScriptFunction;
import org.nrg.dicom.mizer.service.MizerContext;
import org.nrg.dicom.mizer.service.impl.MizerContextWithScript;
import org.nrg.ecat.edit.ScriptApplicator;
import org.nrg.framework.concurrency.LoggingThreadPoolExecutor;
import org.nrg.framework.constants.PrearchiveCode;
import org.nrg.xnat.upload.ecat.FormatSessionDateFunction;
import org.nrg.xnat.upload.ecat.IndexedSessionLabelFunction;
import org.nrg.xnat.upload.net.HttpExceptionHandler;
import org.nrg.xnat.upload.net.xnat.*;
import org.nrg.xnat.upload.util.VisitTemplateI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static org.nrg.xnat.upload.application.UploadAssistant.getSettings;
import static org.nrg.xnat.upload.data.SessionVariableNames.SESSIONS;

public class Project {
    public Project(final String name) {
        _name = name;
        _sessions = _executor.submit(new ProjectSessionLister(name));
        _subjectLister = new ProjectSubjectLister(name);
        _subjects = _executor.submit(_subjectLister);
        _anonScript = _executor.submit(new AnonScriptRetriever(this));

        final Future<Map<String, String>> sessionsMap = _executor.submit(new ProjectSessionConverter(_sessions));

        _ecatScriptApplicator = _executor.submit(new ECATScriptApplicatorRetriever(name, getEcatFunctions(sessionsMap)));
        _petTracers = _executor.submit(new PETTracerRetriever(name));
        _requireDate = _executor.submit(new RequireDateSettingRetriever(name));
        _prearchiveCode = _executor.submit(new ProjectPreArcCodeRetriever(name));
        _namingConvention = _executor.submit(new NamingConventionRetriever(name));
        _allowCreateSubject = _executor.submit(new AllowCreateSubjectSettingRetriever(name));
        _visitTemplate = _executor.submit(new CrProtocolVisitRetriever(this));
        _sessionMergingSetting = _executor.submit(new SessionMergingSetting());
    }

    class ProjectSessionConverter implements Callable<Map<String, String>> {
        private final Future<ProjectSessionCollection> _sessions;

        public ProjectSessionConverter(Future<ProjectSessionCollection> sessions) {
            _sessions = sessions;
        }

        @Override
        public Map<String, String> call() throws Exception {
            return _sessions.get().toMap();
        }
    }

    /**
     * Adds a (newly created) subject to the project.
     *
     * @param subject The subject to add to the project.
     */
    public void addSubject(final Subject subject) {
        _newSubjects.add(subject);
    }

    public Collection<Subject> getSubjects() throws ExecutionException, InterruptedException {
        // TODO: Optimally this should parse the _subjects Future ONCE then store those results.
        final Map<String, String> subjectMap = _subjects.get();
        _log.info("Got {} subjects for project {}", subjectMap.size(), _name);
        final Collection<Subject> subjects = Sets.newLinkedHashSet();
        for (final Map.Entry<String, String> subjectEntry : subjectMap.entrySet()) {
            _log.info("Adding subject {}:{}", subjectEntry.getKey(), subjectEntry.getValue());
            subjects.add(new Subject(subjectEntry.getKey(), subjectEntry.getValue()));
        }
        if (_log.isInfoEnabled()) {
            if (_newSubjects.size() > 0) {
                _log.info("Adding new subjects {}", Joiner.on(", ").join(_newSubjects));
            }
        }
        subjects.addAll(_newSubjects);
        return subjects;
    }

    public String getSubjectListerUri() {
        return _subjectLister.getUri();
    }

    public boolean hasSubject(final String name) throws ExecutionException, InterruptedException {
        for (final Subject subject : getSubjects()) {
            if (name.equals(subject.getLabel())) {
                return true;
            }
        }
        return false;
    }

    public ProjectSessionCollection getSessionLabels() throws InterruptedException, ExecutionException {
        return _sessions.get();
    }

    public MizerContext getProjectAnonScript() {
        try {
            final MizerContextWithScript context = _anonScript.get();
            // Null context indicates the script is empty or disabled.
            if (context == null) {
                return null;
            }
            context.setElement(SESSIONS, getSessionLabels().toMap());
            return context;
        } catch (InterruptedException | ExecutionException e) {
            _log.error("An error occurred trying to retrieve the anonymization script for the project " + _name, e);
        }
        return null;
    }

    public ScriptApplicator getEcatScriptApplicator() throws InterruptedException, ExecutionException {
        return _ecatScriptApplicator.get();
    }

    public Set<String> getPETTracers() throws InterruptedException, ExecutionException {
        return _petTracers.get();
    }

    public Boolean getRequireDateSetting() throws InterruptedException, ExecutionException {
        return _requireDate.get();
    }

    public VisitTemplateI getVisitTemplate() throws InterruptedException, ExecutionException {
        return _visitTemplate.get();
    }

    public String getSessionNamingConvention() throws InterruptedException, ExecutionException {
        return _namingConvention.get();
    }

    public PrearchiveCode getPrearchiveCode() throws InterruptedException, ExecutionException {
        return _prearchiveCode.get();
    }

    public Boolean allowCreateSubject() {
        try {
            return _allowCreateSubject.get();
        } catch (Exception e) {
            return true;
        }
    }

    public List<String> getSessionMergingSettings() throws ExecutionException, InterruptedException {
        return _sessionMergingSetting.get();
    }

    /**
     * Submits a job to this project's executor service.
     *
     * @param <T>      Return type for the callable method.
     * @param callable The callable to passed to the executor.
     */
    public <T> void submit(final Callable<T> callable) {
        _executor.submit(callable);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return _name;
    }

    private static Map<String, ? extends ScriptFunction> getEcatFunctions(final Future<Map<String, String>> sessions) {
        final Map<String, ScriptFunction> ecatFunctionMap = Maps.newHashMap();
        ecatFunctionMap.put("makeSessionLabel", new IndexedSessionLabelFunction(sessions));
        ecatFunctionMap.put("formatSessionDate", new FormatSessionDateFunction(new Callable<Session>() {
            public Session call() throws Exception {
                return getSettings().getCurrentSession();
            }
        }));
        return ecatFunctionMap;
    }

    private static final Logger _log = LoggerFactory.getLogger(Project.class);

    private final ExecutorService _executor    = LoggingThreadPoolExecutor.newCachedThreadPool(new HttpExceptionHandler(404));
    private final Set<Subject>    _newSubjects = Sets.newLinkedHashSet();    // locally added subjects

    private final String                           _name;
    private final ProjectSubjectLister             _subjectLister;
    private final Future<Map<String, String>>      _subjects;
    private final Future<ProjectSessionCollection> _sessions;
    private final Future<MizerContextWithScript>   _anonScript;
    private final Future<ScriptApplicator>         _ecatScriptApplicator;
    private final Future<Set<String>>              _petTracers;
    private final Future<Boolean>                  _requireDate;
    private final Future<PrearchiveCode>           _prearchiveCode;
    private final Future<VisitTemplateI>           _visitTemplate;
    private final Future<String>                   _namingConvention;
    private final Future<Boolean>                  _allowCreateSubject;
    private final Future<List<String>>             _sessionMergingSetting;
}
