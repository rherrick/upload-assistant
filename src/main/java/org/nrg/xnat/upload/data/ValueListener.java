/*
 * upload-assistant: org.nrg.xnat.upload.data.ValueListener
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.data;

public interface ValueListener {
    void hasChanged(SessionVariable variable);
    void isInvalid(SessionVariable variable, Object value, String message);
}
