/*
 * upload-assistant: org.nrg.xnat.upload.ecat.EcatSessionVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ecat;

import org.nrg.dicom.mizer.exceptions.MultipleInitializationException;
import org.nrg.dicom.mizer.values.ConstantValue;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.variables.Variable;
import org.nrg.xnat.upload.data.AbstractSessionVariable;
import org.nrg.xnat.upload.data.SessionVariable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Set;

import static org.nrg.dicom.mizer.values.Value.EMPTY_VARIABLES;

public abstract class EcatSessionVariable extends AbstractSessionVariable implements SessionVariable {
    /**
     * @param variable The _variable to set for the ECAT session.
     */
    public EcatSessionVariable(final Variable variable) {
        super(variable.getName());
        _variable = variable;
    }

    public static SessionVariable getSessionVariable(final Variable v) {
        return new TextEcatVariable(v);
    }

    @Override
    public Value getValue() {
        logger.trace("getting value for " + this);
        final Value value = _variable.getValue();
        if (value != null) {
            return value;
        }
        final Value initialValue = _variable.getInitialValue();
        if (initialValue == null) {
            return null;
        }
        logger.trace("no value set; evaluating initial value " + initialValue);
        try {
            final String evaluated = initialValue.on(Collections.<Integer, String>emptyMap());
            logger.trace("initial value = " + evaluated);
            return evaluated == null ? null : new ConstantValue(evaluated);
        } catch (Throwable t) {
            logger.warn("unable to evaluate initial value " + initialValue, t);
            return null;
        }
    }

    @Override
    public String setValue(final String value) throws InvalidValueException {
        return _variable.setValue(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setInitialValue(final Value initialValue) throws MultipleInitializationException {
        _variable.setInitialValue(initialValue);
    }

    @Override
    public boolean isHidden() {
        return _variable.isHidden();
    }


    public Set<Variable> getVariables() {
        final Value iv = _variable.getInitialValue();
        return null == iv ? EMPTY_VARIABLES : iv.getVariables();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return super.toString() + " (" + _variable + ")";
    }

    private final Logger logger = LoggerFactory.getLogger(EcatSessionVariable.class);
    private final Variable _variable;
}
