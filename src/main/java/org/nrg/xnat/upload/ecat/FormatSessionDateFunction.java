/*
 * upload-assistant: org.nrg.xnat.upload.ecat.FormatSessionDateFunction
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ecat;

import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.scripts.ScriptFunction;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.variables.Variable;
import org.nrg.ecat.edit.AbstractEcatMizerValue;
import org.nrg.xnat.upload.data.Session;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Callable;

public final class FormatSessionDateFunction implements ScriptFunction {
    public FormatSessionDateFunction(final Callable<Session> sessionSource) {
        this._sessionSource = sessionSource;
    }

    /* (non-Javadoc)
     * @see org.nrg.ecat.edit.ScriptFunction#apply(java.util.List)
     */
    @SuppressWarnings("unchecked")
    public Value apply(final List args)
            throws ScriptEvaluationException {
        try {
            return new DelayedSessionDateValue((Value)args.get(0));
        } catch (Exception e) {
            throw new ScriptEvaluationException("unable to extract argument for formatSessionData function", e);
        }
    }

    private final class DelayedSessionDateValue extends AbstractEcatMizerValue {
        DelayedSessionDateValue(final Value format) {
            _format = format;
        }

        @SuppressWarnings("unchecked")
        public Set<Variable> getVariables() {
            return _format.getVariables();
        }

        @Override
        public void replace(final Variable variable) {
            //
        }

        @Override
        public SortedSet<Long> getTags() {
            return EMPTY_TAGS;
        }

        @SuppressWarnings("unchecked")
        public String on(final Map<Integer, String> m) throws ScriptEvaluationException {
            final String format = _format.on(m);
            final Session session;
            try {
                session = _sessionSource.call();
            } catch (Exception e) {
                throw new ScriptEvaluationException("unable to retrieve session", e);
            }
            if (null == session) {
                return null;
            }
            final Date d = session.getDateTime();
            return null == d ? null : new SimpleDateFormat(format).format(d);
        }

        private final Value _format
                ;
    }

    private final Callable<Session> _sessionSource;
}
