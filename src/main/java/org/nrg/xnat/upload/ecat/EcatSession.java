/*
 * upload-assistant: org.nrg.xnat.upload.ecat.EcatSession
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ecat;

import com.google.common.base.Strings;
import com.google.common.collect.*;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONException;
import org.netbeans.spi.wizard.ResultProgressHandle;
import org.netbeans.spi.wizard.Summary;
import org.nrg.dicom.mizer.service.MizerContext;
import org.nrg.dicom.mizer.variables.Variable;
import org.nrg.ecat.MatrixDataFile;
import org.nrg.ecat.edit.ScriptApplicator;
import org.nrg.xnat.upload.application.UploadAssistantSettings;
import org.nrg.xnat.upload.data.*;
import org.nrg.xnat.upload.dcm.Study;
import org.nrg.xnat.upload.net.*;
import org.nrg.xnat.upload.ui.UploadResultPanel;
import org.nrg.xnat.upload.util.Messages;
import org.nrg.xnat.upload.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.nrg.xnat.upload.application.UploadAssistant.getCurrentXnatServer;
import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;
import static org.nrg.xnat.upload.application.UploadAssistant.getSettings;
import static org.nrg.xnat.upload.data.SessionVariableNames.PROTOCOL_LABEL;
import static org.nrg.xnat.upload.data.SessionVariableNames.VISIT_LABEL;
import static org.nrg.xnat.upload.ui.UIUtils.buildCommitEntity;

public final class EcatSession implements Session {
    private final JComponent _parent;

    @SuppressWarnings("unchecked")
    public EcatSession(final JComponent parent, final Collection<MatrixDataFile> files) {
        _parent = parent;
        final List<MatrixDataFile> list = Lists.newArrayList(files);
        Collections.sort(list);
        this.first = files.isEmpty() ? null : list.get(0);
        this.data = Collections.unmodifiableCollection(list);
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getAccession()
     */
    public String getAccession() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getDateTime()
     */
    public Date getDateTime() {

        // The first time this is called, the user is prompted to enter the timezone in which this
        // session was acquired. Subsequent calls re-use the timezone that was selected.
        final String message = Messages.getMessage("ecatsession.selecttimezone");

        if (timeZone == null) {
            final JComboBox<String> list = new JComboBox<>(timezones);
            final JPanel panel = new JPanel();
            final JLabel label = new JLabel(message);

            panel.add(label);
            panel.add(list);

            JOptionPane.showMessageDialog(_parent, panel, message, JOptionPane.PLAIN_MESSAGE);
            timeZone = TimeZone.getTimeZone(timezones[list.getSelectedIndex()]);
        }
        DateTime original = new DateTime(first.getDate().getTime());
        DateTime zoned = original.withZone(DateTimeZone.forTimeZone(timeZone));
        return zoned.toDate();
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getDescription()
     */
    public String getDescription() {
        return first.getDescription();
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getFileCount()
     */
    public int getFileCount() {
        return data.size();
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getFormat()
     */
    public String getFormat() {
        return FORMAT;
    }

    @Override
    public List<SessionVariable> getVariables(final List<MizerContext> contexts) {
        return null;
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getID()
     */
    public String getID() {
        return first.getPatientID();
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getModalities()
     */
    public Set<String> getModalities() {
        return Collections.singleton(MODALITY);
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getScanCount()
     */
    public int getScanCount() {
        return data.size();
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getTimeZone()
     */
    public TimeZone getTimeZone() {
        return timeZone;
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getSize()
     */
    public long getSize() {
        if (0 == size) {
            for (final MatrixDataFile f : data) {
                size += f.getSize();
            }
        }
        return size;
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#getVariables(java.util.Map)
     */
    @SuppressWarnings("unchecked")
    public List<SessionVariable> getVariables(final Project project) {
        final List<Variable> evs;
        try {
            final ScriptApplicator applicator = project.getEcatScriptApplicator();
            if (null == applicator) {
                logger.info("no script available");
                return Collections.emptyList();
            } else {
                evs = applicator.getSortedVariables();
            }
        } catch (Throwable t) {
            logger.warn("unable to load script applicator", t);
            return Collections.emptyList();
        }
        final List<SessionVariable> sessionVars = Lists.newArrayList();
        sessionVars.add(new AssignedSessionVariable(SessionVariableNames.MODALITY_LABEL, MODALITY, true));
        for (final Variable ev : evs) {
            sessionVars.add(EcatSessionVariable.getSessionVariable(ev));
        }
        return sessionVars;
    }

    /**
     * Provides a study identifier that is as unique and verbose as possible.
     *
     * @return The study identifier.
     *
     * @see java.lang.Object#toString()
     */
    public String toString() {
        Date sessionDate = getDateTime();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm z");
        if (timeZone != null) {
            sdf.setTimeZone(timeZone);
        }

        final StringBuilder builder = new StringBuilder("ECAT study ");
        final String studyId = getID();
        if (null != studyId) {
            builder.append(studyId);
        }
        final Object accessionNumber = getAccession();
        //noinspection ConstantConditions
        if (null != accessionNumber) {
            builder.append(" (").append(accessionNumber).append(")");
        }
        if (null != sessionDate) {
            builder.append(" ").append(sdf.format(sessionDate));
        }
        final Object description = getDescription();
        if (null != description) {
            builder.append(" ").append(description);
        }
        //noinspection ConstantConditions
        if (null == studyId && null == accessionNumber) {
            builder.append(" [NO ECAT ID]");
        }
        return builder.toString();
    }

    private static String makeTimestamp() {
        return new SimpleDateFormat(TIMESTAMP_FORMAT).format(new Date());
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.Session#uploadTo(java.util.Map, org.netbeans.spi.wizard.ResultProgressHandle)
     */
    public boolean uploadTo(final Map<?, ?> parameters,
                            final UploadFailureHandler failureHandler,
                            final ResultProgressHandle progress) {
        if (data.isEmpty()) {
            progress.failed("No ECAT files available to upload", true);
            return false;
        }

        final UploadAssistantSettings settings = getSettings();
        final Project project = getCurrentXnatServer().getCurrentProject();
        final Subject subject = settings.getCurrentSubject();
        final String session = settings.getSessionLabel().getValue().asString();

        final Map<String, SessionVariable> variables = settings.getSessionVariables();
        final String visit = variables.containsKey(VISIT_LABEL) ? variables.get(VISIT_LABEL).getValue().asString() : null;
        final String protocol = variables.containsKey(PROTOCOL_LABEL) ? variables.get(PROTOCOL_LABEL).getValue().asString() : null;

        final String timestamp = makeTimestamp();

        progress.setBusy("Creating session " + session);

        final String uploadPath = String.format("/data/projects/%s/subjects/%s/experiments/%s?xsiType=xnat:petSessionData", project, subject, session);

        // create session
        final String sessionID;
        try {
            final StringResponseProcessor processor = new StringResponseProcessor();
            getRestServer().doPut(uploadPath, processor);
            sessionID = processor.toString();
        } catch (ConflictHttpException e) {
            progress.failed(e.getMessage(), true);
            return false;
        } catch (Exception e) {
            logger.warn("An unexpected exception occurred calling " + uploadPath, e);
            progress.failed(e.getMessage(), true);    // TODO: try again?
            return false;
        }

        final StringBuffer path = new StringBuffer(String.format("/data/services/import?dest=/prearchive/projects/%s/%s/%s", project, timestamp, session));

        if (!Strings.isNullOrEmpty(visit)) {
            path.append("&VISIT=").append(visit);
        }
        if (!Strings.isNullOrEmpty(protocol)) {
            path.append("&PROTOCOL=").append(protocol);
        }
        if (!Strings.isNullOrEmpty(timeZone.getID())) {
            path.append("&TIMEZONE=").append(timeZone.getID());
        }
        path.append("&SOURCE=uploader");

        // add scans to session, and the data file to each scan
        int index = 0;
        final int size = data.size();

        for (final Iterator<MatrixDataFile> iterator = data.iterator(); iterator.hasNext(); ) {
            final MatrixDataFile dataFile = iterator.next();
            final File f = dataFile.getFile();
            while (true) {
                try {
                    final String scan = String.format("%s&SUBJECT_ID=%s&overwrite=append&xnat:petSessionData/scans/scan/ID=%d", path, subject.getLabel(), ++index);
                    progress.setProgress(String.format("Uploading scan %d/%d", index, size), index - 1, size);
                    final HttpURLConnectionProcessor processor = new ModifyingUploadProcessor(f, progress, project.toString(), subject.getLabel(), sessionID);
                    logger.trace("uploading {} as scan {}", f, index);
                    final long start = new Date().getTime();
                    getRestServer().doPost(scan, processor);
                    logger.trace("upload successful: {} bytes in {} seconds", size, (new Date().getTime() - start) / 1000L);
                    break;
                } catch (Throwable t) {
                    if (failureHandler.shouldRetry(dataFile.getFile(), t)) {
                        logger.info("upload failed, retrying", t);
                    } else {
                        final Map<File, Object> failures = Maps.newLinkedHashMap();
                        failures.put(f, t);
                        final StringBuilder message = new StringBuilder("user canceled operation after errors:");
                        message.append(LINE_SEPARATOR);
                        buildFailureMessage(message, failures);
                        progress.failed(message.toString(), true);
                        while (iterator.hasNext()) {
                            failures.put(iterator.next().getFile(), getUserCanceledFailure());
                        }
                        return false;
                    }
                }
            }
        }

        final Map<?, ?> failures = Maps.newHashMap();
        closeSession(String.format("/data/prearchive/projects/%s/%s/%s", project, timestamp, session), session, parameters, progress, failures);
        return true;
    }

    // TODO: this is (almost) duplicate of method in Study
    private void closeSession(final String path, final String session, final Map<?, ?> params, final ResultProgressHandle progress, final Map<?, ?> failures) {
        final URL baseURL = getCurrentXnatServer().getValidatedUrl();

        // Close session and return result
        try {
            if (failures.isEmpty()) {
                progress.setBusy("Committing session");
                logger.trace("committing session {}", path);
                final JSONRequestConnectionProcessor handler = new JSONRequestConnectionProcessor(buildCommitEntity());

                String queryParams = "?action=commit&SOURCE=uploader";
                //add visit
                if (null != params.get(SessionVariableNames.VISIT_LABEL) && !Strings.isNullOrEmpty(((AssignedSessionVariable) params.get(SessionVariableNames.VISIT_LABEL)).getValue().asString())) {
                    queryParams += "&VISIT=" + ((AssignedSessionVariable) params.get(SessionVariableNames.VISIT_LABEL)).getValue().asString();
                }
                //add protocol
                if (null != params.get(SessionVariableNames.PROTOCOL_LABEL) && !Strings.isNullOrEmpty(((AssignedSessionVariable) params.get(SessionVariableNames.PROTOCOL_LABEL)).getValue().asString())) {
                    queryParams += "&PROTOCOL=" + ((AssignedSessionVariable) params.get(SessionVariableNames.PROTOCOL_LABEL)).getValue().asString();
                }
                //add timeZone
                if (null != timeZone) {
                    queryParams += "&TIMEZONE=" + timeZone.getID();
                }

                getRestServer().doPost(path + queryParams, handler);

                final String resultPath = Study.getWebAppRelativePath(baseURL, handler.getResponseEntity());
                final URL result = new URL(baseURL + "/" + resultPath);

                // TODO: build summary, notify user
                final UploadResultPanel resultPanel = new UploadResultPanel(session, Study.buildSessionViewURL(result, resultPath));
                progress.finished(Summary.create(resultPanel, path));
            } else {
                progress.failed(Study.buildFailureMessage(failures), false);
            }
        } catch (JSONException e) {
            logger.error("unable to write commit request entity", e);
        } catch (HttpException e) {
            logger.error("session commit failed", e);
            switch (e.getResponseCode()) {
                case HttpURLConnection.HTTP_NOT_FOUND: {
                    progress.failed(Messages.getMessage("error.http.notfound", Study.formatServer(baseURL), baseURL.getPath()), true);
                    return;
                }

                case HttpURLConnection.HTTP_INTERNAL_ERROR: {
                    progress.failed(Messages.getMessage("error.http.internal", Study.formatServer(baseURL)), true);
                    return;
                }

                case HttpURLConnection.HTTP_CONFLICT: {
                    progress.failed(Messages.getMessage("error.http.conflict", Study.formatServer(baseURL)), true);
                    return;
                }

                default: {
                    progress.failed(Messages.getMessage("error.http.unknown", e.getResponseCode(), e.getMessage()), true);
                }
            }
        } catch (IOException e) {
            logger.error("Session commit failed", e);
            progress.failed(Messages.getMessage("error.io.exception", Study.formatServer(baseURL), e.getMessage()), false);
        } catch (Throwable t) {
            logger.error("Session commit failed", t);
            progress.failed(Messages.getMessage("error.total.failure", t.getMessage()), false);
        }
    }

    private Object getUserCanceledFailure() {
        return "User canceled upload";
    }

    private static <T> void buildFailureMessage(final StringBuilder sb, final Map<File, T> failures) {
        final Multimap<T, File> inverse = LinkedHashMultimap.create();
        Multimaps.invertFrom(Multimaps.forMap(failures), inverse);
        final Multimap<Object, File> causes = Utils.consolidateKeys(inverse, 4);
        for (final Object key : causes.keySet()) {
            final Collection<File> files = causes.get(key);
            sb.append(files.size()).append(" files not uploaded: ").append(key);
            sb.append(LINE_SEPARATOR);
        }
    }

    private static final String LINE_SEPARATOR   = System.getProperty("line.separator");
    private static final String FORMAT           = "ECAT";
    private static final String MODALITY         = "PET";
    private static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";

    private final Logger logger = LoggerFactory.getLogger(EcatSession.class);
    private final MatrixDataFile             first;
    private final Collection<MatrixDataFile> data;
    private long size = 0;

    private TimeZone timeZone = null;

    private static final String[] timezones = new String[]{
            System.getProperty("user.timezone"),
            "America/New_York",
            "America/Chicago",
            "America/Los_Angeles",
            "Pacific/Midway",
            "US/Hawaii",
            "US/Alaska",
            "US/Pacific",
            "America/Tijuana",
            "US/Arizona",
            "America/Chihuahua",
            "US/Mountain",
            "America/Guatemala",
            "US/Central",
            "America/Mexico_City",
            "Canada/Saskatchewan",
            "America/Bogota",
            "US/Eastern",
            "US/East-Indiana",
            "Canada/Eastern",
            "America/Caracas",
            "America/Manaus",
            "America/Santiago",
            "Canada/Newfoundland",
            "Brazil/East",
            "America/Buenos_Aires",
            "America/Godthab",
            "America/Montevideo",
            "Atlantic/South_Georgia",
            "Atlantic/Azores",
            "Atlantic/Cape_Verde",
            "Africa/Casablanca",
            "Europe/London",
            "Europe/Berlin",
            "Europe/Belgrade",
            "Europe/Brussels",
            "Europe/Warsaw",
            "Africa/Algiers",
            "Asia/Amman",
            "Europe/Athens",
            "Asia/Beirut",
            "Africa/Cairo",
            "Africa/Harare",
            "Europe/Helsinki",
            "Asia/Jerusalem",
            "Europe/Minsk",
            "Africa/Windhoek",
            "Asia/Baghdad",
            "Asia/Kuwait",
            "Europe/Moscow",
            "Africa/Nairobi",
            "Asia/Tbilisi",
            "Asia/Tehran",
            "Asia/Muscat",
            "Asia/Baku",
            "Asia/Yerevan",
            "Asia/Kabul",
            "Asia/Yekaterinburg",
            "Asia/Karachi",
            "Asia/Calcutta",
            "Asia/Colombo",
            "Asia/Katmandu",
            "Asia/Novosibirsk",
            "Asia/Dhaka",
            "Asia/Rangoon",
            "Asia/Bangkok",
            "Asia/Krasnoyarsk",
            "Asia/Hong_Kong",
            "Asia/Irkutsk",
            "Asia/Kuala_Lumpur",
            "Australia/Perth",
            "Asia/Taipei",
            "Asia/Tokyo",
            "Asia/Seoul",
            "Asia/Yakutsk",
            "Australia/Adelaide",
            "Australia/Darwin",
            "Australia/Brisbane",
            "Australia/Sydney",
            "Pacific/Guam",
            "Australia/Hobart",
            "Asia/Vladivostok",
            "Asia/Magadan",
            "Pacific/Auckland",
            "Pacific/Fiji",
            "Pacific/Tongatapu"
    };
}
