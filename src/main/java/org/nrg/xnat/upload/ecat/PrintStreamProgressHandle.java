/*
 * upload-assistant: org.nrg.xnat.upload.ecat.PrintStreamProgressHandle
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ecat;

import java.awt.Container;
import java.io.PrintStream;

import org.netbeans.spi.wizard.ResultProgressHandle;

class PrintStreamProgressHandle implements ResultProgressHandle {
	private final PrintStream out;
	private boolean running = true;
	
	PrintStreamProgressHandle(final PrintStream out) {
		this.out = out;
	}
	
	/* (non-Javadoc)
	 * @see org.netbeans.spi.wizard.ResultProgressHandle#addProgressComponents(java.awt.Container)
	 */
	public void addProgressComponents(final Container panel) {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.netbeans.spi.wizard.ResultProgressHandle#failed(java.lang.String, boolean)
	 */
	public void failed(final String message, final boolean canNavigateBack) {
		out.println("upload FAILED: " + message);
		running = false;
	}

	/* (non-Javadoc)
	 * @see org.netbeans.spi.wizard.ResultProgressHandle#finished(java.lang.Object)
	 */
	public void finished(final Object result) {
		out.println("upload succeeded");
		running = false;
	}

	/* (non-Javadoc)
	 * @see org.netbeans.spi.wizard.ResultProgressHandle#isRunning()
	 */
	public boolean isRunning() { return running; }

	/* (non-Javadoc)
	 * @see org.netbeans.spi.wizard.ResultProgressHandle#setBusy(java.lang.String)
	 */
	public void setBusy(final String description) {
		out.println(description);
	}

	/* (non-Javadoc)
	 * @see org.netbeans.spi.wizard.ResultProgressHandle#setProgress(int, int)
	 */
	public void setProgress(final int currentStep, final int totalSteps) { }

	/* (non-Javadoc)
	 * @see org.netbeans.spi.wizard.ResultProgressHandle#setProgress(java.lang.String, int, int)
	 */
	public void setProgress(final String description,
			final int currentStep, final int totalSteps) {
		out.println(String.format("%s (%d/%d)", description, currentStep, totalSteps));
	}
}
