package org.nrg.xnat.upload.net;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.nrg.dicom.mizer.service.impl.MizerContextWithScript;
import org.nrg.framework.concurrency.LoggingThreadPoolExecutor;
import org.nrg.framework.generics.GenericUtils;
import org.nrg.xnat.upload.data.Project;
import org.nrg.xnat.upload.net.xnat.AnonScriptRetriever;
import org.nrg.xnat.upload.net.xnat.BuildInfoRetriever;
import org.nrg.xnat.upload.ui.UIUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;
import static org.nrg.xnat.upload.ui.UIUtils.VERIFY_XNAT_PATH;

/**
 * Contains data about an XNAT server instance.
 */
public class XnatServer {
    @JsonCreator
    public XnatServer(@JsonProperty("address") final String address, @JsonProperty("allowUnverifiedHttps") final boolean allowUnverifiedHttps, @JsonProperty("username") final String username) {
        this(address, allowUnverifiedHttps, username, null);
    }

    public XnatServer(final String address, final boolean allowUnverifiedHttps, final String username, final char[] password) {
        _address = address.endsWith("/") ? StringUtils.removeEnd(address, "/") : address;
        if (!isExplicitlyNonSsl(address)) {
            _allowUnverifiedHttps = allowUnverifiedHttps;
        } else {
            _allowUnverifiedHttps = null;
        }

        if (StringUtils.isNotBlank(username)) {
            _username = username;
            _password = password;
        } else {
            final PasswordAuthentication userInfo = getUserInfo(address);
            if (userInfo == null) {
                _username = null;
                _password = null;
            } else {
                _username = userInfo.getUserName();
                _password = userInfo.getPassword();
            }
        }
        _name = UIUtils.formatXnatServerName(_address, _username);
    }

    public List<String> retrieveProjects() throws IOException {
        final List<String> projects = GenericUtils.convertToTypedList(getRestServer().getValues(PROJECTS_REST_PATH, "id"), String.class);
        if (_log.isInfoEnabled()) {
            _log.info("Retrieved {} projects from server at {}: {}", projects.size(), getAddress(), Joiner.on(", ").join(projects));
        }

        // Initialize the project map with null values.
        _projects.clear();
        for (final String project : projects) {
            _projects.put(project, null);
        }

        return projects;
    }

    @JsonIgnore
    public List<String> getProjects() throws IOException {
        return _projects.isEmpty() ? retrieveProjects() : new ArrayList<>(_projects.keySet());
    }

    @JsonIgnore
    public Project getProject(final String projectId) {
        if (!_projects.containsKey(projectId)) {
            return null;
        }

        final Project project = _projects.get(projectId);
        if (project != null) {
            return project;
        }

        _projects.put(projectId, new Project(projectId));

        return _projects.get(projectId);
    }

    @JsonIgnore
    public Project getCurrentProject() {
        return getProject(getCurrentProjectId());
    }

    public String getCurrentProjectId() {
        return _currentProjectId;
    }

    public void setCurrentProjectId(final String projectId) {
        _currentProjectId = projectId;
    }

    /**
     * Verifies the server data from the submitted connection. This includes {@link #setValidatedUrl(URL) setting the
     * validated URL} (meaning that the protocol is fixed for servers where the protocol wasn't explicitly specified),
     * {@link #setSessionCookies(HttpURLConnection) caching the session cookies} for future transactions during the
     * upload process, {@link #setUsingUnverifiedHttps(HttpURLConnection) setting the unverified HTTPS flag}, and {@link
     * #getSiteWideAnonScript() retrieving the site-wide anonymization script}.
     *
     * @param connection The HTTP connection object from a successful connection to the XNAT server.
     */
    public void verify(final HttpURLConnection connection) {
        setValidatedUrl(connection.getURL());
        setSessionCookies(connection);
        setUsingUnverifiedHttps(connection);
        _buildInfo = _executor.submit(new BuildInfoRetriever());
        _siteWideAnonScript = _executor.submit(new AnonScriptRetriever());
    }

    /**
     * Gets the name of the server configuration. The name can't be set and is calculated based on the username and
     * server address without the protocol. For example, the URL <b>https://xnatdev.xnat.org</b> and username
     * <b>admin</b> would give a name like "admin @ xnatdev.xnat.org".
     *
     * @return Returns the server name.
     */
    @JsonIgnore
    public String getName() {
        return _name;
    }

    /**
     * Gets the URL for the configured server address.
     *
     * @return Returns the URL for the server.
     */
    public String getAddress() {
        return _address;
    }

    /**
     * Gets the validated URL for the configured server address. This takes the value set for {@link #getAddress()} and
     * tests for protocol and response.
     *
     * @return Returns the validated URL for the server.
     */
    @JsonIgnore
    public URL getValidatedUrl() {
        return _validatedUrl;
    }

    /**
     * Sets the validated URL for the configured server address.
     *
     * @param validatedUrl Sets the validated URL for the server.
     */
    public void setValidatedUrl(final URL validatedUrl) {
        if (validatedUrl.toString().endsWith(VERIFY_XNAT_PATH)) {
            try {
                _validatedUrl = new URL(validatedUrl.toString().replace(VERIFY_XNAT_PATH, ""));
            } catch (MalformedURLException e) {
                _log.error("Can't create a valid URL from {} after stripping {} from the URL path", validatedUrl.toString(), VERIFY_XNAT_PATH);
            }
        } else {
            _validatedUrl = validatedUrl;
        }
    }

    /**
     * Gets the user for the configured server.
     *
     * @return Returns the username for the server.
     */
    public String getUsername() {
        return _username;
    }

    /**
     * Gets the "allow unverified HTTPS" flag. This lets transactions ignore warnings about expired, self-signed, or
     * untrusted certificates when calling secure connections. This should only be used in development environments or
     * in circumstances where you are certain that the security certificate has not been compromised.
     *
     * If the {@link #getAddress()} address for the server instance} uses HTTP rather than HTTPS, this method always
     * returns null.
     *
     * @return Returns <b>true</b> for servers using the HTTPS protocol if unverified HTTPS transactions are allowed and
     * <b>false</b> if not. Returns null if the URL is configured to use HTTP.
     */
    public Boolean isAllowUnverifiedHttps() {
        return _allowUnverifiedHttps;
    }

    /**
     * Indicates whether the connection to the server was made using unverified HTTPS. Like {@link
     * #isAllowUnverifiedHttps()},
     * this method returns null when the URL does not use the HTTPS protocol. This is set to <b>true</b> when {@link
     * #isAllowUnverifiedHttps()} is set to <b>true</b> <i>and</i> the connection to the server required failing over
     * to
     * an unverified HTTPS connection.
     *
     * @return Returns <b>true</b> if the server required using unverified HTTPS to connect.
     */
    @JsonIgnore
    public Boolean getUsingUnverifiedHttps() {
        return _usingUnverifiedHttps;
    }

    /**
     * Sets whether the connection was made using unverified HTTPS.
     *
     * @param connection The connection to set the unverified HTTPS flag from.
     */
    public void setUsingUnverifiedHttps(final HttpURLConnection connection) {
        _usingUnverifiedHttps = _allowUnverifiedHttps != null && _allowUnverifiedHttps && RestServer.isUnverifiedConnection(connection);
    }

    /**
     * Gets the password. Unlike the other properties on the server object, the password is transient, i.e. it is not
     * persisted with the server definition when saved.
     *
     * @return The password for accessing the XNAT server.
     */
    @JsonIgnore
    public char[] getPassword() {
        return _password;
    }

    /**
     * Sets the password. Unlike the other properties on the server object, the password is transient, i.e. it is not
     * persisted with the server definition when saved.
     *
     * @param password The password for accessing the XNAT server.
     */
    public void setPassword(final char[] password) {
        _password = password;
    }

    /**
     * Returns a password authentication object containing the username and password. Note that if either the username
     * or the password is blank, this method returns <b>null</b>.
     *
     * @return A password authentication object if the username and password are both specified, <b>null</b> otherwise.
     */
    @JsonIgnore
    public PasswordAuthentication getPasswordAuthentication() {
        if (StringUtils.isNotBlank(_username) && ArrayUtils.isNotEmpty(_password)) {
            return new PasswordAuthentication(_username, _password);
        }
        return null;
    }

    /**
     * Gets the saved session cookies for the current server.
     *
     * @return A list of saved session cookies.
     */
    @JsonIgnore
    public List<HttpCookie> getSessionCookies() {
        return Lists.newArrayList(_cookies.values());
    }

    /**
     * Sets the session cookies for the current server from the submitted connection. Any cookies on the connection that
     * already exist in the server's session cookie store will be overwritten.
     *
     * @param connection The connection from which cookies should be pulled.
     */
    public void setSessionCookies(final HttpURLConnection connection) {
        final Map<String, List<String>> fields  = connection.getHeaderFields();
        final List<String>              cookies = Lists.newArrayList();
        if (fields.containsKey("Cookie")) {
            cookies.addAll(fields.get("Cookie"));
        }
        if (fields.containsKey("Set-Cookie")) {
            cookies.addAll(fields.get("Set-Cookie"));
        }
        if (cookies.size() > 0) {
            for (final String cookie : cookies) {
                final List<HttpCookie> parsedCookies = HttpCookie.parse(cookie);
                for (final HttpCookie parsedCookie : parsedCookies) {
                    _cookies.put(parsedCookie.getName(), parsedCookie);
                }
            }
        }
    }

    /**
     * Indicates whether the server currently has a session ID cookie. Note that this doesn't guarantee that the session
     * cookie is still valid!
     *
     * @return Returns <b>true</b> if a session ID cookie is found, <b>false</b> otherwise.
     */
    public boolean hasSessionId() {
        return _cookies.containsKey("JSESSIONID");
    }

    /**
     * Sets the submitted session ID as the current session ID cookie for the server.
     *
     * @param jSessionId The session ID to set.
     */
    @SuppressWarnings("unused")
    public void setSessionId(final String jSessionId) {
        _cookies.put("JSESSIONID", new HttpCookie("JSESSIONID", jSessionId));
    }

    /**
     * Gets the server version for the current configured XNAT server. If an error occurs while trying to retrieve the
     * version, this method will return an empty string.
     *
     * @return The version of the current configured XNAT server.
     */
    @JsonIgnore
    public String getServerVersion() {
        try {
            final Map<String, String> properties = _buildInfo.get();
            if (properties.containsKey("version")) {
                return properties.get("version");
            }
        } catch (InterruptedException | ExecutionException e) {
            _log.warn("An error occurred trying to get the server version.", e);
        }
        return "";
    }

    /**
     * Gets the site-wide anonymization script for the current configured XNAT server.
     *
     * @return The site-wide anonymization script.
     */
    @JsonIgnore
    public Future<MizerContextWithScript> getSiteWideAnonScript() {
        return _siteWideAnonScript;
    }

    /**
     * Clears any saved site-wide anonymization script to allow for re-querying the server.
     */
    @SuppressWarnings("unused")
    public void refreshSiteWideAnonScript() {
        _siteWideAnonScript = null;
    }

    /**
     * Checks whether the submitted information matches the current server configuration.
     *
     * @param url                  The URL to test.
     * @param allowUnverifiedHttps The unverified HTTPS flag to test.
     * @param username             The username to test.
     *
     * @return Returns true if the URL, unverified HTTPS flag, and username match the current server configuration
     * <i>if</i> the URL protocol is "https". Otherwise returns true if the URL and username match the current server
     * configuration.
     */
    public boolean matches(final String url, final Boolean allowUnverifiedHttps, final String username) {
        // We always test address and username.
        final EqualsBuilder builder = new EqualsBuilder().append(_address, url).append(_username, username);

        // Only test the allowUnverifiedHttps flag if the URL is secure.
        if (!isExplicitlyNonSsl(_address)) {
            builder.append(_allowUnverifiedHttps, allowUnverifiedHttps);
        }

        // Now evaluate the equals.
        return builder.isEquals();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object object) {
        if (object == null) {
            return false;
        }

        if (this == object) {
            return true;
        }

        if (!(object instanceof XnatServer)) {
            return false;
        }

        final XnatServer other = (XnatServer) object;
        return matches(other.getAddress(), other.isAllowUnverifiedHttps(), other.getUsername());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getAddress())
                .append(isAllowUnverifiedHttps())
                .append(getUsername())
                .append(getName())
                .toHashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return getName();
    }

    /**
     * Tries to parse a URL from the submitted string and, if successful, returns any user information that might be
     * encoded on the URL.
     *
     * @param url The URL text to parse.
     *
     * @return A password authentication instance containing the username and password.
     */
    private PasswordAuthentication getUserInfo(final String url) {
        try {
            final String  userInfo = new URL(url).getUserInfo();
            final Matcher matcher  = USER_INFO_PATTERN.matcher(userInfo);
            if (matcher.matches()) {
                return new PasswordAuthentication(matcher.group(1), matcher.group(2).toCharArray());
            }
        } catch (MalformedURLException ignored) {
            // This is OK: it might just mean that there's no protocol specified.
        }
        return null;
    }

    private static boolean isExplicitlyNonSsl(final String address) {
        return StringUtils.isNotBlank(address) && address.matches("^http://.*$");
    }

    private static final Pattern USER_INFO_PATTERN  = Pattern.compile("(?<username>[^:@/]*):(?<password>[^:@]*)");
    private static final String  PROJECTS_REST_PATH = "/data/projects?permissions=edit&dataType=xnat:subjectData";

    private static final Logger          _log      = LoggerFactory.getLogger(XnatServer.class);
    private static final ExecutorService _executor = LoggingThreadPoolExecutor.newCachedThreadPool(new HttpExceptionHandler(404));

    private final String                         _address;
    private       URL                            _validatedUrl;
    private final Boolean                        _allowUnverifiedHttps;
    private       Boolean                        _usingUnverifiedHttps;
    private final String                         _username;
    private final String                         _name;
    private       char[]                         _password;
    private       Future<Map<String, String>>    _buildInfo;
    private       Future<MizerContextWithScript> _siteWideAnonScript;
    private       String                         _currentProjectId;

    private final Map<String, Project>    _projects = Maps.newHashMap();
    private final Map<String, HttpCookie> _cookies  = Maps.newHashMap();
}
