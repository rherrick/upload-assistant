/*
 * upload-assistant: org.nrg.xnat.upload.net.JSONRequestConnectionProcessor
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.nio.charset.Charset;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.ByteStreams;

public class JSONRequestConnectionProcessor implements HttpURLConnectionProcessor {
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String MEDIA_TYPE = "application/json";

    private final Logger logger = LoggerFactory.getLogger(JSONRequestConnectionProcessor.class);
    private final JSONObject request;
    private final ByteArrayOutputStream response = new ByteArrayOutputStream();

    public JSONRequestConnectionProcessor(final JSONObject request) {
        this.request = request;
    }

    /* (non-Javadoc)
     * @see HttpURLConnectionProcessor#prepare(java.net.HttpURLConnection)
     */
    public void prepare(final HttpURLConnection connection) throws IOException {
        connection.setDoOutput(true);
        connection.addRequestProperty(CONTENT_TYPE_HEADER, MEDIA_TYPE);

        connection.connect();
        try (final OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream())) {
            request.write(writer);
        } catch (Throwable t) {
            System.out.println("failure");
            t.printStackTrace();
            logger.debug("stream copy failed", t);
        }
    }

    /* (non-Javadoc)
     * @see HttpURLConnectionProcessor#process(java.net.HttpURLConnection)
     */
    public void process(final HttpURLConnection connection) throws IOException {
        try (final InputStream input = connection.getInputStream()) {
            ByteStreams.copy(input, response);
        }
    }
    
    /**
     * Returns the content of the response entity as a String
     * @return response entity content
     */
    public String getResponseEntity() {
        return new String(response.toByteArray(), Charset.defaultCharset());
    }
}
