package org.nrg.xnat.upload.net.xnat;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class ProjectSessionCollection {
    final List<ProjectSession> sessions = Lists.newArrayList();
    Map<String, String> map = null;

    public void add(ProjectSession ps) {
        sessions.add(ps);
    }

    public List<ProjectSession> getSessions() {
        return sessions;
    }

    public Map<String, String> toMap() {
        if (map == null) {
            map = Maps.newHashMap();
            for (ProjectSession ps : sessions) {
                map.put((StringUtils.isEmpty(ps.getLabel())) ? ps.getId() : ps.getLabel(), ps.getId());
            }
        }

        return map;
    }
}
