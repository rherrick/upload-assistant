package org.nrg.xnat.upload.net.xnat;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.nrg.xnat.upload.net.JSONDecoder;

import java.util.Map;
import java.util.concurrent.Callable;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public final class SessionScanCount implements Callable<Integer> {
    private final String project;
    private final String label;

    public SessionScanCount(String project, String label) {
        this.project = project;
        this.label = label;
    }

    public Integer call() {
        final SessionExtractor processor = new SessionExtractor();
        try {
            getRestServer().doGet("/data/experiments?format=json&columns=label,xnat:experimentData/meta/status,xnat:imageSessionData/scans/scan/ID&label=" + label + "&project=" + project, processor);

            Map<String, Integer> counts = processor.get();

            if (counts.size() == 0 || counts.get(label) == null) {
                return 0;
            } else {
                return counts.get(label);
            }
        } catch (Throwable t) {
            return 0;
        }
    }

    private static final class SessionExtractor implements JSONDecoder {
        Map<String, Integer> map = Maps.newHashMap();

        /*
         * (non-Javadoc)
         * @see org.nrg.net.RestOpManager.JSONDecoder#decode(org.json.JSONObject)
         */
        public void decode(final JSONObject o) throws JSONException {
            String label = o.optString("label");
            if (StringUtils.isEmpty(label)) {
                return;
            }

            if (!map.containsKey(label)) {
                map.put(label, 0);
            }

            String scan = o.optString("xnat:imagesessiondata/scans/scan/id");
            if (!StringUtils.isEmpty(scan)) {
                map.put(label, map.get(label) + 1);
            }
        }

        public Map<String, Integer> get() {
            return map;
        }
    }
}
