package org.nrg.xnat.upload.net.xnat;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nrg.xnat.upload.data.Project;
import org.nrg.xnat.upload.net.HttpException;
import org.nrg.xnat.upload.net.StringResponseProcessor;
import org.nrg.xnat.upload.util.VisitTemplate;
import org.nrg.xnat.upload.util.VisitTemplateI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public class CrProtocolVisitRetriever implements Callable<VisitTemplateI> {
    public CrProtocolVisitRetriever(final Project project) {
        _projectPath = String.format(URL_TEMPLATE_PROJECT, project.toString());
        _project = project;
    }

    public VisitTemplateI call() {
        try {
            final VisitTemplate defaultVisit = getDefaultVisit();
            if (defaultVisit == null) {
                return null;
            }

            final StringResponseProcessor processor = new StringResponseProcessor();
            getRestServer().doGet(String.format("/data/config/cr_protocol/templates/%s?contents=true&accept-not-found=true", defaultVisit.getKey()), processor);

            final JSONArray visits = new JSONArray(processor.toString());
            defaultVisit.setVisits(visits);
            logger.debug("Visit template downloaded with " + defaultVisit.getVisits().size() + " visits");

            return defaultVisit;
        } catch (JSONException t) {
            logger.error("", t);
            return null;
        } catch (HttpException t) {
            if (t.getResponseCode() == 404) {
                logger.debug("No site-wide visit template configured");
                return null;
            } else {
                logger.error("", t);
                return null;
            }
        } catch (Throwable t) {
            logger.error("", t);
            //default to not required
            return null;
        }
    }

    private VisitTemplate getDefaultVisit() throws Exception {
        try {
            final VisitTemplate defaultVisit = getVisitTemplate(_projectPath);
            // check to see if we got back a status page instead of the setting
            // if so, there's no project specific list, so just get the site wide setting
            if (defaultVisit == null || StringUtils.equals(defaultVisit.getKey(), "DEFAULT")) {
                logger.debug("project specific visit template is DEFAULT");
                throw new Exception("Use the site-wide script");
            } else if (StringUtils.isBlank(defaultVisit.getKey())) {
                logger.debug("project specific visit template is empty");
                return null;
            } else {
                logger.debug("project specific visit template is " + defaultVisit.getKey());
                return defaultVisit;
            }
        } catch (Throwable t) {
            logger.debug("failed to retrieve project specific visit template, querying site-wide default");
        }

        return getVisitTemplate(URL_TEMPLATE_SITE);
    }

    private VisitTemplate getVisitTemplate(final String path) throws Exception {
        final StringResponseProcessor processor = new StringResponseProcessor();
        getRestServer().doGet(path, processor);
        final String jsonText = processor.toString();
        return StringUtils.isNotBlank(jsonText) ? new VisitTemplate(new JSONObject(jsonText), _project) : null;
    }

    private static final String URL_TEMPLATE_SITE    = "/data/config/cr_protocol/default?contents=true&accept-not-found=true";
    private static final String URL_TEMPLATE_PROJECT = "/data/projects/%s/config/cr_protocol/template?contents=true&accept-not-found=true";
    private static final Logger logger               = LoggerFactory.getLogger(CrProtocolVisitRetriever.class);

    private final String  _projectPath;
    private final Project _project;
}
