/*
 * upload-assistant: org.nrg.xnat.upload.net.xnat.AbstractBooleanSettingRetriever
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.net.xnat;

import org.apache.commons.lang3.StringUtils;
import org.nrg.xnat.upload.net.StringResponseProcessor;

import java.util.concurrent.Callable;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public abstract class AbstractBooleanSettingRetriever implements Callable<Boolean> {
    protected AbstractBooleanSettingRetriever(final String project) {
        _projectPath = String.format(getProjectUrlTemplate(), project);
    }

    public Boolean call() {
        final Boolean project = getBooleanSetting(_projectPath);
        if (project != null) {
            return project;
        }
        final Boolean site = getBooleanSetting(getSiteUrl());
        return site != null ? site : getDefaultReturnValue();
    }

    private Boolean getBooleanSetting(final String path) {
        final StringResponseProcessor processor = new StringResponseProcessor();
        try {
            getRestServer().doGet(path, processor);

            final String setting = processor.toString().trim();
            return StringUtils.isBlank(setting) ? null : StringUtils.equalsIgnoreCase("true", setting);
        } catch (Throwable t) {
            return null;
        }
    }

    protected abstract boolean getDefaultReturnValue();
    protected abstract String getProjectUrlTemplate();
    protected abstract String getSiteUrl();

    private final String _projectPath;
}
