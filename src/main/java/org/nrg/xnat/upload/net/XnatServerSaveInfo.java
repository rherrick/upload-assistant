package org.nrg.xnat.upload.net;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public enum XnatServerSaveInfo {
    Nothing("Nothing"),
    Url("URL"),
    UrlAndUsername("URL and username");

    XnatServerSaveInfo(final String display) {
        _display = display;
    }

    @Override
    public String toString() {
        return _display;
    }

    public static XnatServerSaveInfo fromDisplay(final String display) {
        if (!_isInit) {
            cacheSaveInfo();
        }
        if (StringUtils.isBlank(display)) {
            return UrlAndUsername;
        }
        return _registry.get(display);
    }

    public static String[] options() {
        if (!_isInit) {
            cacheSaveInfo();
        }
        return _displays.toArray(new String[_displays.size()]);
    }

    private static void cacheSaveInfo() {
        if (_registry.isEmpty()) {
            synchronized (XnatServerSaveInfo.class) {
                for (final XnatServerSaveInfo saveInfo : values()) {
                    _registry.put(saveInfo.toString(), saveInfo);
                    _displays.add(saveInfo.toString());
                }
            }
            _isInit = true;
        }
    }

    private static       boolean                         _isInit   = false;
    private static final Map<String, XnatServerSaveInfo> _registry = new HashMap<>();
    private static final SortedSet<String>               _displays = new TreeSet<>();

    private String _display;
}
