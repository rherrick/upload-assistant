/*
 * upload-assistant: org.nrg.xnat.upload.io.ecat.EcatTrawler
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.io.ecat;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.nrg.ecat.MatrixDataFile;
import org.nrg.framework.io.EditProgressMonitor;
import org.nrg.xnat.upload.data.Session;
import org.nrg.xnat.upload.ecat.EcatSession;
import org.nrg.xnat.upload.io.Trawler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class EcatTrawler implements Trawler {
    public EcatTrawler(final JComponent parent) {
        _parent = parent;
    }

    @Override
    public Collection<Session> trawl(final Iterator<File> files, final Collection<File> remaining, final EditProgressMonitor progressMonitor) {
        final Multimap<String, MatrixDataFile> filesets = ArrayListMultimap.create();
        while (files.hasNext()) {
            if (progressMonitor != null && progressMonitor.isCanceled()) {
                return new ArrayList<>();
            }
            final File file = files.next();
            logger.trace("checking {}", file);
            try {
                final MatrixDataFile matrixDataFile = new MatrixDataFile(file);
                filesets.put(matrixDataFile.getPatientID(), matrixDataFile);
            } catch (IOException e) {
                logger.debug("Had a problem reading the file {}: {}", file.getAbsolutePath(), e.getMessage());
                if (remaining != null) {
                    remaining.add(file);
                }
            }
        }

        logger.trace("Found {} ECAT sessions: {}", filesets.size(), filesets);
        final List<Session> sessions = new ArrayList<>();
        for (final String label : filesets.keySet()) { // add in sorted order
            sessions.add(new EcatSession(_parent, filesets.get(label)));
        }
        return sessions;
    }

    private static final Logger logger = LoggerFactory.getLogger(EcatTrawler.class);

    private final JComponent _parent;
}
