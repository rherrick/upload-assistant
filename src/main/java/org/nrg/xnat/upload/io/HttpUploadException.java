/*
 * upload-assistant: org.nrg.xnat.upload.io.HttpUploadException
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.io;

import java.io.IOException;
import java.net.HttpURLConnection;

public final class HttpUploadException extends Exception {
    private static final long serialVersionUID = 1L;
    private final int statusCode;
    private final String entity;

    public HttpUploadException(final int statusCode, final String message, final String entity) {
        super(message);
        this.statusCode = statusCode;
        this.entity = entity;
    }

    public HttpUploadException(final HttpURLConnection connection) throws IOException {
        this(connection.getResponseCode(), connection.getResponseMessage(),
                HttpUtils.readEntity(connection));
    }

    public int getStatusCode() { return statusCode; }

    public String getEntity() { return entity; }
    
    public String toString() {
        return "HTTP error " + getStatusCode() + " - " + getMessage() + "<br>" + getEntity();
    }
}
