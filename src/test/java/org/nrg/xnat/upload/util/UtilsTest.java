/*
 * upload-assistant: org.nrg.xnat.upload.util.UtilsTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.util;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

public class UtilsTest {

	/**
	 * Test method for {@link Utils#slurp(java.lang.StringBuilder, java.io.InputStream)}.
	 */
	@Test
	public void testSlurpStringBuilderInputStream() throws IOException {
		final String input = "foo bar baz";
		final InputStream in = new ByteArrayInputStream(input.getBytes());
		final StringBuilder sb = new StringBuilder("a");
		Utils.slurp(sb, in);
		assertEquals("a" + input, sb.toString());
	}

	/**
	 * Test method for {@link Utils#slurp(java.io.InputStream)}.
	 */
	@Test
	public void testSlurpInputStream() throws IOException {
		final String input = "foo bar baz";
		final InputStream in = new ByteArrayInputStream(input.getBytes());
		assertEquals(input, Utils.slurp(in));
	}

	/**
	 * Test method for {@link Utils#getCanonicalFile(java.io.File)}.
	 */
	@Test
	public void testGetCanonicalFile() throws IOException {
		final File f1 = mock(File.class);
		final File fc = mock(File.class);
		when(f1.getCanonicalFile()).thenReturn(fc);
		assertEquals(fc, Utils.getCanonicalFile(f1));

		final File f2 = mock(File.class);
		final File fa = mock(File.class);
		when(f2.getCanonicalFile()).thenThrow(new IOException());
		when(f2.getAbsoluteFile()).thenReturn(fa);
		assertEquals(fa, Utils.getCanonicalFile(f2));
	}

	/**
	 * Test method for {@link Utils#showNearestUnitFraction(java.lang.StringBuilder, long, long, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testShowNearestUnitFraction() {
		assertEquals("0.00/0.00 x", Utils.showNearestUnitFraction(new StringBuilder(), 0, 0, "x").toString());
		assertEquals("0.00/100 x", Utils.showNearestUnitFraction(new StringBuilder(), 0, 100, "x").toString());
		assertEquals("0.488/0.977 kx", Utils.showNearestUnitFraction(new StringBuilder(), 500, 1000, "x").toString());
		assertEquals("0.00/0.909 Tx", Utils.showNearestUnitFraction(new StringBuilder(), 2112, 1000000000000L, "x").toString());
	}

	/**
	 * Test method for {@link Utils#showNearestUnits(java.lang.StringBuilder, long, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testShowNearestUnitsStringBuilderLongStringString() {
		assertEquals("0.00 x", Utils.showNearestUnits(new StringBuilder(), 0, "x").toString());
		assertEquals("1.00 x", Utils.showNearestUnits(new StringBuilder(), 1, "x").toString());
		assertEquals("99.0 x", Utils.showNearestUnits(new StringBuilder(), 99, "x").toString());
		assertEquals("921 x", Utils.showNearestUnits(new StringBuilder(), 921, "x").toString());
		// 1024 * 0.9 = 921.6
		assertEquals("0.900 kx", Utils.showNearestUnits(new StringBuilder(), 922, "x").toString());
		assertEquals("1.00 kx", Utils.showNearestUnits(new StringBuilder(), 1024, "x").toString());
		assertEquals("922 kx", Utils.showNearestUnits(new StringBuilder(), 943718, "x").toString());
		assertEquals("0.900 Mx", Utils.showNearestUnits(new StringBuilder(), 943719, "x").toString());
		assertEquals("922 Mx", Utils.showNearestUnits(new StringBuilder(), 966367641, "x").toString());
		assertEquals("0.900 Gx", Utils.showNearestUnits(new StringBuilder(), 966367642, "x").toString());
		assertEquals("922 Gx", Utils.showNearestUnits(new StringBuilder(), 989560464998L, "x").toString());
		assertEquals("0.900 Tx", Utils.showNearestUnits(new StringBuilder(), 989560464999L, "x").toString());
		assertEquals("922 Tx", Utils.showNearestUnits(new StringBuilder(), 1013309916158361L, "x").toString());
		assertEquals("0.900 Px", Utils.showNearestUnits(new StringBuilder(), 1013309916158362L, "x").toString());
	}

	/**
	 * Test method for {@link Utils#showNearestUnits(java.lang.StringBuilder, long, java.lang.String)}.
	 */
	@Test
	public void testShowNearestUnitsStringBuilderLongString() {
		assertEquals("0.0 x", Utils.showNearestUnits(new StringBuilder(), 0, "x", "%.2g").toString());
		assertEquals("0.90 kx", Utils.showNearestUnits(new StringBuilder(), 922, "x", "%.2g").toString());
		assertEquals("0.000 x", Utils.showNearestUnits(new StringBuilder(), 0, "x", "%.4g").toString());
		assertEquals("0.9004 kx", Utils.showNearestUnits(new StringBuilder(), 922, "x", "%.4g").toString());
	}
}
